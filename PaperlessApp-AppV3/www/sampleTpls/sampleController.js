/// <reference path="../../.vscode/typings/angularjs/angular.d.ts" />

(function(app) {
    app.controller('sampleController', sampleController);
    
    sampleController.$inject = ['sampleService'];
    function sampleController(sampleService) {
        var $ctrl = this;

        /**
         * Variables areas.
         */

        /**
         * Helper methods.
         */
        
        /**
         * $ctrl areas.
         */
        $ctrl.create = function () {
            var jsonData = { 'name': $ctrl.name, 'address': $ctrl.address };
            sampleService.create(jsonData).then(
                function(response) {},
                function(error) {}
            ).finally();
        };

        $ctrl.readById = function () {
            var id = $ctrl.id;
            sampleService.readById(id).then(
                function(response) {},
                function(error) {}
            ).finally();
        };

        $ctrl.updateById = function () {
            var id = $ctrl.id;
            sampleService.updateById(id).then(
                function(response) {},
                function(error) {}
            ).finally();
        };

        $ctrl.deleteById = function () {
            var id = $ctrl.id;
            sampleService.deleteById(id).then(
                function(response) {},
                function(error) {}
            ).finally();
        };        

        /**
         * Starting point.
         */
         $ctrl.init = function() {

         };
    }
})(angular.module('common.core'));