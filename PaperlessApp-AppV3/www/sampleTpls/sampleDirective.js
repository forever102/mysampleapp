/// <reference path="../../.vscode/typings/angularjs/angular.d.ts" />

(function(app) {
    app.directive('sampleDirective', sampleDirective);
    function sampleDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'sampleTpls/sampleDirective.html',
            scope: {
                text: '='
            },
            link: linkFunc,
            controller: sampleDirectiveCtrl,
            // note: This would be 'ExampleController' (the exported controller name, as string)
            // if referring to a defined controller in its separate file.
            controllerAs: '$ctrl',
            bindToController: true, // because the scope is isolated.
            replace: true
        };
        
        return directive;

        function linkFunc(scope, el, attr, ctrl) { }
    }

    sampleDirectiveCtrl.$inject = [];
    function sampleDirectiveCtrl() {
        var $ctrl = this;
        $ctrl.title = 'sample template directive.';
    }
})(angular.module('common.core'));