/// <reference path="../../.vscode/typings/angularjs/angular.d.ts" />

(function(app) {
    app.component('sampleComponent', {
        templateUrl: 'sampleTpls/sampleComponent.html',
        bindings: {
            text: '='
        },
        controller: sampleComponentCtrl
    });

    sampleComponentCtrl.$inject = [];
    function sampleComponentCtrl() {
        var $ctrl = this;
        $ctrl.title = 'sample component template';

        $ctrl.$onInit = function() { }
    }
})(angular.module('common.core'));