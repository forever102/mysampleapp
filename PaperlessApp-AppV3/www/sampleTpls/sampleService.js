/// <reference path="../../.vscode/typings/angularjs/angular.d.ts" />

(function (app) {
    app.factory('sampleService', sampleService);

    sampleService.$inject = ['$http', 'logger'];
    function sampleService($http, logger) {
        /**
         * Variables areas.
         */
        var baseUrl = '';

        /**
         * functions areas.
         */
        function create(jsonData) {
            return $http.post(this.baseUrl + '/api/create', jsonData);
        }
        function readById(id) {
            return $http.get(this.baseUrl + '/api/readById', { 'id': id });
        }
        function updateById(id) {
            return $http.post(this.baseUrl + '/api/updateById', { 'id': id });
        }
        function deleteById(id) {
            return $http.get(this.baseUrl + '/api/deleteById', { 'id': id });
        }
        
        /**
         * Starting point.
         */
        return {
            create: create,
            readById: readById,
            updateById: updateById,
            deleteById: deleteById
        };
    }
})(angular.module('common.core'));