/// <reference path="../../../../.vscode/typings/angularjs/angular.d.ts"/>

(function (app) {
    app.component('progressbar', {
        templateUrl: 'shared/components/progressBar/progressbar.html',
        bindings: {},
        controller: progressBarCtrl
    });

    progressBarCtrl.$inject = ['$scope', 'sectionRepo'];
    function progressBarCtrl($scope, sectionRepo) {
        var $ctrl = this;

        $ctrl.progressPercentValue = 0;
        $ctrl.$onInit = function () {
        }

        /**
         * Listen to parent events.
         */
        $scope.$on('updateProgressBarEvent', function (event, data) {
            //  if ( data.sections.length > 0) {
            //     var completedSections = 0;
            //     for (var i = 0; i < data.sections.length; i++) {
            //         var section = data.sections[i];
            //         if (section.numberOfEmptyAnswer != section.totalQuestions) {
            //             completedSections = completedSections + 1;
            //         }
            //     }
            //     $ctrl.progressPercentValue = (completedSections * 100) / data.sections.length;
            // }

            console.info('updateProgressBarEvent is fired.');
            if (data.resultId) {
                var completedSections = 0;                
                return sectionRepo.readByResultId(data.resultId).then(
                    function (res) {
                        for (var i = 0; i < res.rows.length; i++) {
                            var section = res.rows.item(i);
                            if (section.numberOfEmptyAnswer != section.totalQuestions) {
                                completedSections = completedSections + 1;
                            }
                        }
                        $ctrl.progressPercentValue = (completedSections * 100) / res.rows.length;
                        console.info('$ctrl.progressPercentValue is: ' + $ctrl.progressPercentValue);
                    },
                    function (err) { }
                );
            }
        });
    }
})(angular.module('common.core'));