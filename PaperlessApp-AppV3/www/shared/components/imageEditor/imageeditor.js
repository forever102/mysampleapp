/// <reference path="../../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.component('imageeditor', {
        // templateUrl: 'shared/components/imageEditor/imageeditor.html',
        bindings: { },
        controller: imageeditorCtrl
    });

    imageeditorCtrl.$inject = ['$scope', '$ionicModal', 'cameraService', '$ionicLoading'];
    function imageeditorCtrl($scope, $ionicModal, cameraService, $ionicLoading) {
        var $ctrl = this;        

        var canvas = null;
        var rotateImageDegree = 0;

        /**
         * Helper methods.
         */
        function initModal(editingImageName, qId) {
            $ionicModal.fromTemplateUrl('shared/components/imageEditor/imageeditor.html', {
                id: 'image-editor-modal',
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $ctrl.editingImageName = editingImageName ? editingImageName : null;
                $ctrl.editingQuestionId = qId;
                $ctrl.imageEditorModal = modal;
                $ctrl.imageEditorModal.show();
                initCanvas();
            });
        };
        function initCanvas() {
            width = window.innerWidth; // width of canvas.
            // height = width * (4 / 3); // height of canvas.
            height = window.innerHeight;

            canvas = new fabric.Canvas('imageCanvas');
            
            if(canvas) {
                canvas.selection = false;
                fabric.Object.prototype.selectable = false; // prevent drawing objects to be draggable or clickable

                // sets canvas height and width
                canvas.setHeight(height);
                canvas.setWidth(width);
                // sets canvas height and width
                // *** having both canvas.setHeight and canvas.width prevents errors when saving
                canvas.width = width;
                canvas.height = height;

                // canvas.backgroundColor = 'black';

                // canvas.isDrawingMode = false;
                // canvas.freeDrawingBrush.width = 6; // size of the drawing brush
                // $ctrl.brushcolor = '#000000'; // set brushcolor to black to begin
                                                
                if ($ctrl.editingImageName && $ctrl.editingImageName != '') {
                    var imageUrl = $ctrl.urlForImage($ctrl.editingImageName);
                    // var imageUrl = 'img/testImg.jpg'; // For testing purpose.
                    
                    // Set a background image using a url.
                    // canvas.setBackgroundImage(imageUrl, canvas.renderAll.bind(canvas), {
                    //     height: height,
                    //     width: width,
                    //     top: canvas.getCenter().top,
                    //     left: canvas.getCenter().left,
                    //     originX: 'center',
                    //     originY: 'center'
                    // });

                    // Set image in canvas.
                    fabric.Image.fromURL(imageUrl, function(oImg) {
                        oImg.scaleToWidth(canvas.getWidth());
                        oImg.scaleToHeight(canvas.getHeight());
                        canvas.add(oImg);
                        canvas.centerObject(canvas.item(0));
                    });
                }                
            }                    
        };        
        function urlForImageName(imageName) {
            if (!imageName || imageName == '') return null;
            else return cordova.file.dataDirectory + imageName;
        }        
        function rotateImage(rotateImageDegree) {
            canvas.item(0).setAngle(rotateImageDegree);
            canvas.item(0).scaleToWidth(canvas.getWidth());
            canvas.item(0).scaleToHeight(canvas.getHeight());
            canvas.centerObject(canvas.item(0));
            canvas.renderAll();

            // canvas.backgroundImage.setAngle(rotateImageDegree);
            // canvas.backgroundImage.scaleToWidth(window.innerWidth);
            // canvas.backgroundImage.scaleToHeight(window.innerHeight);
            // canvas.renderAll();
        }

        /**
         * $ctrl area.
         */
        $ctrl.urlForImage = function(imageName) {
            if (!imageName || imageName == '') return null;

            var imagePath = cordova.file.dataDirectory + imageName;
            // console.log('imagePath is: ' + imagePath);

            return imagePath;
        };        
        $ctrl.cancelEditImage = function() {
            $ctrl.imageEditorModal.remove();
            // $ctrl.editingImageUrl = null;
        };
        $ctrl.updateImage = function(editingImageName, editingQuestionId) {
            if(rotateImageDegree != 0) {
                var base64ImageString = canvas.toDataURL();
                var editingImageUrl = urlForImageName(editingImageName);
                // console.log('editingImageUrl is: ' + editingDrawingImageUrl);                
                $ionicLoading.show({
                    template: `<p>Updating image...</p><ion-spinner></ion-spinner>`
                }).then(function(){
                    cameraService.updateImage(editingQuestionId, base64ImageString, editingImageUrl, function() { 
                        $ionicLoading.hide().then(function() {
                            $ctrl.imageEditorModal.remove();
                            rotateImageDegree = 0;
                        });
                    });
                });
            }
        };
        $ctrl.deleteImage = function(editingImageName, editingQuestionId) {
            cameraService.deleteImage($ctrl.urlForImage(editingImageName), editingQuestionId,
                function () {},
                function (error) {}
            );
            $ctrl.imageEditorModal.remove();
        };
        $ctrl.rotateRight = function() {
            rotateImageDegree = rotateImageDegree + 90;
            if (rotateImageDegree > 360) rotateImageDegree = 90;
            rotateImage(rotateImageDegree);
        };
        $ctrl.rotateLeft = function() {
            rotateImageDegree = rotateImageDegree - 90;
            if (rotateImageDegree < -360) rotateImageDegree = -90;
            rotateImage(rotateImageDegree);
        };        

        /**
         * Listen to broadcast events from parent.
         */
        $scope.$on('showImageEditorEvent', function(event, obj) {
            initModal(obj.imageName, obj.questionId);
        });
        // To make it simple, I lock screen orientation when the drawing pad is shown.
        $scope.$on('modal.shown', function(event, modal) {
            if (modal.id == 'image-editor-modal') {
                var orientation = (window.orientation == -90 || window.orientation == 90) ? 'landscape' : 'portrait';
                screen.lockOrientation(orientation);
            }
        });
        $scope.$on('modal.removed', function(event, modal) {
            if (modal.id == 'image-editor-modal') {
                screen.unlockOrientation();
            }
        });

        /**
         * Starting point.
         */
        $ctrl.$onInit = function() { 
            $ctrl.showBar = true;
        }
    }
})(angular.module('common.core'));