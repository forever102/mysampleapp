/**
 * Source:
 * - https://github.com/isaacabdel/ionic-fabric
 */
(function (app) {
    'use strict';

    app.component('drawingPad', {
        templateUrl: '',
        bindings: {},
        controller: drawingPadCtrl
    });        

    drawingPadCtrl.$inject = ['$ionicLoading', '$scope', '$ionicModal', '$ionicPopover', '$ionicPopup', 'drawingService'];
    function drawingPadCtrl($ionicLoading, $scope, $ionicModal, $ionicPopover, $ionicPopup, drawingService) {
        var $ctrl = this;

        // Stuffs for canvas.
        var width, height; // width of canvas.
        var canvas = null;
        var rotateDrawringDegree = 0;
        
        /**
         * Helper methods.
         */
        function urlForImageName(imageName) {
            if (!imageName || imageName == '') return null;
            else return cordova.file.dataDirectory + imageName;
        }

        /**
         * Scopes area.
         */
        $ctrl.initModal = function (editingImageName, qId, allowEdit) {
            // $ionicModal.fromTemplateUrl('app/directives/drawingPadModal.html', {
            $ionicModal.fromTemplateUrl('shared/components/drawingPad/drawingPad.html', {
                id: 'drawing-pad-modal',
                // scope: $scope.externalCtrlScope,
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                // $scope.externalCtrlScope.drawingPadModal = modal;
                $ctrl.drawingPadModal = modal;
                $ctrl.editingImageName = editingImageName ? editingImageName : null;
                $ctrl.editingQuestionId = qId;
                $ctrl.allowEdit = allowEdit;
                $ctrl.drawingPadModal.show();

                $ctrl.initCanvas();
            });            
        };

        $ctrl.initCanvas = function() {
            width = window.innerWidth; // width of canvas.
            // height = width * (4 / 3); // height of canvas.
            height = window.innerHeight;

            canvas = new fabric.Canvas('c');
            
            if(canvas) {
                canvas.selection = false;
                fabric.Object.prototype.selectable = false; // prevent drawing objects to be draggable or clickable

                // sets canvas height and width
                canvas.setHeight(height);
                canvas.setWidth(width);
                // sets canvas height and width
                // *** having both canvas.setHeight and canvas.width prevents errors when saving
                canvas.width = width;
                canvas.height = height;

                canvas.isDrawingMode = false;
                if ($ctrl.allowEdit) canvas.isDrawingMode = true;
                $ctrl.canvas = canvas;
                canvas.freeDrawingBrush.width = 6; // size of the drawing brush
                $ctrl.brushcolor = '#000000'; // set brushcolor to black to begin

                // Set a background image using a url.
                if ($ctrl.editingImageName && $ctrl.editingImageName != '') {
                    var imageUrl = urlForImageName($ctrl.editingImageName);
                    // var imageUrl = 'img/testImg.jpg'; // For testing purpose.
                    canvas.setBackgroundImage(imageUrl, canvas.renderAll.bind(canvas), {
                        height: height,
                        width: width
                    });
                }

                // color options popover
                // $ionicPopover.fromTemplateUrl('views/colors.html', {
                $ionicPopover.fromTemplateUrl('shared/components/drawingPad/drawingPadColors.html', {
                    scope: $scope
                }).then(function (popover) {
                    $ctrl.popover = popover;
                });
            }                    
        };        
        // drawing mode
        $ctrl.drawingMode = function () {
            // check if fabric is in drawing mode
            if (canvas.isDrawingMode == true) {
                // if fabric is in drawing mode, exit drawing mode
                // $ctrl.showColorPaletteIcon = false; // hind color palette icon
                canvas.isDrawingMode = false;
            } else {
                // if fabric is not in drawing mode, enter drawing mode
                // $ctrl.showColorPaletteIcon = true; // show color palette icon
                canvas.isDrawingMode = true;
            }
        }
        $ctrl.openColorsPopover = function ($event) {
            $ctrl.popover.show($event);
        };
        $ctrl.closeColorsPopover = function () {
            $ctrl.popover.hide();
        };

        // list of colors
        $ctrl.colors = [
            { color: "#ecf0f1" },
            { color: "#95a5a6" },
            { color: "#bdc3c7" },
            { color: "#7f8c8d" },
            { color: "#000000" },
            { color: "#F1A9A0" },
            { color: "#D2527F" },
            { color: "#f1c40f" },
            { color: "#f39c12" },
            { color: "#e67e22" },
            { color: "#d35400" },
            { color: "#e74c3c" },
            { color: "#c0392b" },
            { color: "#6D4C41" },
            { color: "#3E2723" },
            { color: "#1abc9c" },
            { color: "#16a085" },
            { color: "#2ecc71" },
            { color: "#27ae60" },
            { color: "#3498db" },
            { color: "#2980b9" },
            { color: "#34495e" },
            { color: "#2c3e50" },
            { color: "#9b59b6" },
            { color: "#8e44ad" },
        ]

        $ctrl.changeBrushColor = function (color) {
            canvas.freeDrawingBrush.color = color;
            $ctrl.brushcolor = color; // used to change the color palatte icon's color
            $ctrl.popover.hide(); // hide popover
        }

        // undo last object, drawing or text
        $ctrl.undoLastObject = function () {
            var canvas_objects = canvas._objects;
            var last = canvas_objects[canvas_objects.length - 1];
            canvas.remove(last);
            canvas.renderAll();
        }

        $ctrl.addText = function () {
            cordova.plugins.Keyboard.disableScroll(false); // This fixes Keyboard hides popup input.
            // prevent user from being in drawing mode while adding text
            // this allows the user to manipulate the text object without drawing at the same time
            canvas.isDrawingMode = false; // exit drawing
            // $ctrl.showColorPaletteIcon = false; // hide color palette icon

            $ctrl.data = {}

            // ionic popup used to prompt user to enter text
            var myPopup = $ionicPopup.show({
                template: '<input type="text" ng-model="$ctrl.data.input">',
                title: 'Enter Text',
                subTitle: '',
                scope: $scope,
                buttons: [
                    {text: 'Cancel'}, 
                    {
                        text: '<b>Save</b>',
                        type: 'button-stable',
                        onTap: function (e) {
                            if (!$ctrl.data.input) {
                                e.preventDefault();                                
                            } else {
                                return $ctrl.data.input;                                
                            }
                        }
                    }
                ]
            });
            myPopup.then(function (input) {
                cordova.plugins.Keyboard.disableScroll(true);

                // fabric js
                if (input) {
                    var t = new fabric.Text(input, {
                        left: (width / 3),
                        top: 100,
                        fontFamily: 'Helvetica',
                        fill: $ctrl.brushcolor, // color
                        selectable: true, // draggbale
                    });
                    canvas.add(t); // add text to fabric.js canvas
                }
            });
        }

        $ctrl.saveDrawing = function (editingImageName, qId) {
            // canvas.toDataURL()
            var canvasData = canvas.toJSON();
            if(canvasData.objects.length > 0 || rotateDrawringDegree != 0) {
                var base64DrawingString = canvas.toDataURL();
                var editingDrawingImageUrl = urlForImageName(editingImageName);
                // console.log('editingDrawingImageUrl is: ' + editingDrawingImageUrl);                
                $ionicLoading.show({
                    template: `<p>Saving drawing...</p><ion-spinner></ion-spinner>`
                }).then(function(){
                    drawingService.saveSignatureImage(qId, base64DrawingString, editingDrawingImageUrl, function() { 
                        $ionicLoading.hide().then(function() {
                            $ctrl.drawingPadModal.remove();
                            rotateDrawringDegree = 0;
                        });
                    });
                });
            }
        };
        $ctrl.cancelDrawing = function () {
            $ctrl.drawingPadModal.remove();
        }
        $ctrl.deleteDrawingImg = function(editingImageName, editingQuestionId) {
            if (editingImageName && editingImageName != '') {
                // console.log('editingImageName is: ' + editingImageName + ', editingQuestionId is: ' + editingQuestionId);
                drawingService.deleteSignatureImage(urlForImageName(editingImageName), editingQuestionId,
                    function () {},
                    function (error) {}
                );
                $ctrl.drawingPadModal.remove();
            }            
        };      
        $ctrl.rotateRight = function() {
            // canvas.isDrawingMode = false;
            rotateDrawringDegree = rotateDrawringDegree + 90;
            if (rotateDrawringDegree > 360) rotateDrawringDegree = 90;
            canvas.backgroundImage.setAngle(rotateDrawringDegree);
            canvas.renderAll();
        };
        $ctrl.rotateLeft = function() {
            // canvas.isDrawingMode = false;
            rotateDrawringDegree = rotateDrawringDegree - 90;
            console.info('rotateDrawringDegree is: ' + rotateDrawringDegree);
            if (rotateDrawringDegree < -360) rotateDrawringDegree = -90;
            canvas.backgroundImage.setAngle(rotateDrawringDegree);
            canvas.renderAll();
        };

        /**
         * Listen to parent events and modal.
         */
        $scope.$on('showDrawingPadModalEvent', function(event, data) {
            $ctrl.initModal(data.editingImageName, data.qId, data.allowEdit);
        });
        // To make it simple, I lock screen orientation when the drawing pad is shown.
        $scope.$on('modal.shown', function(event, modal) {
            if (modal.id == 'drawing-pad-modal') {
                var orientation = (window.orientation == -90 || window.orientation == 90) ? 'landscape' : 'portrait';
                screen.lockOrientation(orientation);
            }
        });
        $scope.$on('modal.removed', function(event, modal) {
            if (modal.id == 'drawing-pad-modal') {
                screen.unlockOrientation();
                $ctrl.closeColorsPopover();
                // $scope.editingSignatureImageUrl = null;
            }
        });
    }
})(angular.module('common.ui'));