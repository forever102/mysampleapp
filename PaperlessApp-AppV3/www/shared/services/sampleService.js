/// <reference path="../../.vscode/typings/angularjs/angular.d.ts"/>

(function (app) {
    app.factory('sampleService', sampleService);

    sampleService.$inject = ['$http', '$rootScope', 'apiService', 'notificationService'];
    function sampleService($http, $rootScope, apiService, notificationService) {
        /**
         * Variables areas.
         */

        /**
         * functions areas.
         */        
        function isUserLoggedIn() {
            return false;
            // return $rootScope.repository.loggedUser != null;
        }
        function login(user, successCb) {
            // To-do: need to use real url from server.
            // apiService.post('/api/account/authenticate', user, successCb, loginFailedCb);
            
            // Simulating server.
            apiService.post('/getUser.json', user, successCb, loginFailedCb);
        }
        function saveCredentials(user) {
            var membershipData = btoa(user.userName + ':' + user.token);
            
            $rootScope.repository = {
                loggedUser: {
                    userName: user.userName,
                    authdata: membershipData
                }
            };

            $http.defaults.headers.common['Authorization'] = 'Basic ' + membershipData;
            // Save to localStorage instead of Sqlite Db.
            window.localStorage.setItem('repository', $rootScope.repository);
        }

        /**
         * Helper functions.
         */
        function loginFailedCb(response) {
            notificationService.displayError(response.data);
        }

        /**
         * Starting point.
         */
        return {
            isUserLoggedIn: isUserLoggedIn,
            login: login,
            saveCredentials: saveCredentials
        };
    }
})(angular.module('common.core'));