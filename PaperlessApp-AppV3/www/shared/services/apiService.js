/// <reference path="../../../.vscode/typings/angularjs/angular.d.ts" />

(function (app) {
    'use strict';

    app.factory('apiService', apiService);

    apiService.$inject = ['$http', '$location', 'notificationService', '$rootScope'];
    function apiService($http, $location, notificationService, $rootScope) {
        // To-do: need to use real url from server.
        // var baseUrl = 'shared/data/sampleData';
        var baseUrl = 'This is secret. Sorry';
        
        function get(url, config, successCb, failureCb, finallyCb) {
            return $http.get(baseUrl + url, config)
                .then(function (result) {
                    successCb(result);
                }, function (error) {
                    if (error.status == '401') {
                        notificationService.displayError('Authentication required.');
                        $rootScope.previousState = $location.path();
                        $location.path('/tab/login');
                    }
                    else if (failureCb != null) {
                        failureCb(error);
                    }
                }).finally(finallyCb);
        }

        function post(url, data, successCb, failureCb, finallyCb) {
            return $http.post(self.baseUrl + url, data)
                .then(function (result) {
                    successCb(result);
                }, function (error) {
                    if (error.status == '401') {
                        notificationService.displayError('Authentication required.');
                        $rootScope.previousState = $location.path();
                        $location.path('/tab/login');
                    }
                    else if (failureCb != null) {
                        failureCb(error);
                    }
                }).finally(finallyCb);
        }

        return {
            get: get,
            post: post
        };
    }

})(angular.module('common.core'));