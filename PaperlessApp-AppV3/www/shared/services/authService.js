/// <reference path="../../../.vscode/typings/angularjs/angular.d.ts"/>

(function (app) {
    app.factory('authService', authService);

    authService.$inject = ['$http', '$rootScope', 'apiService', 'notificationService'];
    function authService($http, $rootScope, apiService, notificationService) {
        /**
         * Variables areas.
         */
        var apiToken = 'This is secret. Sorry';

        /**
         * functions areas.
         */        
        function isUserLoggedIn() {            
            var loggedUser = window.localStorage.getItem('loggedUser');
            
            if (loggedUser) {
                var parsedLoggedUser = JSON.parse(loggedUser);
                $rootScope.loggedUser = parsedLoggedUser;
                return true;                
            }
            else return false;
        }
        function login(user, successCb, finallyCb) {                        
            // Simulating server.
            // apiService.get('/getUser.json', user, successCb, loginFailedCb, finallyCb);

            // To-do: need to use real url from server.
            var data = { 'params': { 'token': apiToken, 'v': '3', 'username': btoa(user.userName), 'password': btoa(user.password) } };
            apiService.get('?action=LogIn', data, successCb, loginFailedCb, finallyCb);
        }
        function saveCredentials(user) {
            var membershipData = btoa(user.userName + ':' + user.token);
            var loggedUser = { 'userName': user.userName, 'authdata': membershipData};
            // $rootScope.repository = {
            //     loggedUser: {
            //         userName: user.userName,
            //         authdata: membershipData
            //     }
            // };

            // $http.defaults.headers.common['Authorization'] = 'Basic ' + membershipData;
            
            // Save to localStorage instead of Sqlite Db.
            // window.localStorage.setItem('repository', $rootScope.repository);
            window.localStorage.setItem('loggedUser', JSON.stringify(loggedUser));
        }

        /**
         * Helper functions.
         */
        function loginFailedCb(response) {
            console.error('response is: ' + JSON.stringify(response));
            notificationService.displayError(response.data, response.status, response.statusText);
        }

        /**
         * Starting point.
         */
        return {
            isUserLoggedIn: isUserLoggedIn,
            login: login,
            saveCredentials: saveCredentials
        };
    }
})(angular.module('common.core'));