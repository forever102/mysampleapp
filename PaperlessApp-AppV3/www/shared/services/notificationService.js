(function (app) {
    'use strict';

    app.factory('notificationService', notificationService);

    notificationService.$inject = ['toastr'];
    function notificationService(toastr) {
        
        function displaySuccess(status, statusText) {
            toastr.success(status, statusText);
        }

        function displayError(error, status, statusText) {
            if (Array.isArray(error)) {
                error.forEach(function (err) {
                    toastr.error(err);
                });
            } else {
                toastr.error(status, statusText);
            }
        }

        function displayWarning(status, statusText) {
            toastr.warning(status, statusText);
        }

        function displayInfo(status, statusText) {
            toastr.info(status, statusText);
        }

        return {
            displaySuccess: displaySuccess,
            displayError: displayError,
            displayWarning: displayWarning,
            displayInfo: displayInfo
        };
    }

})(angular.module('common.core'));