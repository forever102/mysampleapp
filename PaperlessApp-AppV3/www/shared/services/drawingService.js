/// <reference path="../../../.vscode/typings/angularjs/angular.d.ts"/>

(function (app) {
    app.factory('drawingService', drawingService);
    
    drawingService.$inject = ['$rootScope', '$cordovaFile', 'myUtilService', 'resultDetailRepo'];
    function drawingService($rootScope, $cordovaFile, myUtilService, resultDetailRepo) {
        /**
         * Initial stuffs.
         */
        var signatureImageNameArr = {};

        /**
         * Helper methods.
         */
        function saveSignatureImageNameArrByQuestionIdToDb(questionId, imgNameArr, successCb) {
            // To-do: This should be stored in DB by QUESTION_ID.
            // window.localStorage.setItem(qId, JSON.stringify(imgNameArr));
            // successCb();

            if (questionId && $rootScope.currentResultId) {
                var jsonParams = { 'questionId': questionId, 'answeredContent': JSON.stringify(imgNameArr), 'resultId': $rootScope.currentResultId };
                resultDetailRepo.createOrReplace(jsonParams, function(tx, res) {
                    successCb();
                });
            }
        }        

        /**
         * Main methods.
         */        
        function getSignatureImageNameArrByQuestionId(qId, successCb) {            
            // To-do: this must be taken from Sqlite Db by QUESTION_ID.
            // var imgs = window.localStorage.getItem(qId);

            // if (imgs) signatureImageNameArr[qId] = JSON.parse(imgs);       
            // else {
            //     // NOTE: Why do I use array to store only one signature image name for 1 question? That's because this would allow one question able to have multiple signature image names in case they (my boss or clients ^^) want to have it.
            //     signatureImageNameArr[qId] = [];
            // } 

            // return signatureImageNameArr[qId];

            resultDetailRepo.readByQuestionIdAndResultId(qId, $rootScope.currentResultId, function(tx, res) {
                var item = res.rows.item(0);
                if (!item) {
                    // NOTE: Why do I use array to store only one signature image name for 1 question? That's because this would allow one question able to have multiple signature image names in case they (my boss or clients ^^) want to have it.
                    signatureImageNameArr[qId] = [];
                } else {
                    // imagesNames[questionId] = JSON.parse(item.answeredContent);
                    signatureImageNameArr[qId] = JSON.parse(item.answeredContent);
                }                
                successCb(signatureImageNameArr[qId]);
            });
        }        
        function saveSignatureImage(qId, base64SignatureString, editingSignatureImageUrl, successCb) {            
            var isEditing = (editingSignatureImageUrl && editingSignatureImageUrl != '') ? true : false;
            var block = base64SignatureString.split(';'); // Split the base64 string in data and contentType.
            var dataType = block[0].split(':')[1]; // Get the content type. // In this case "image/png".
            var realData = block[1].split(",")[1]; // Get the real base64 content of the file. In this case "iVBORw0KGg....".
            var folderpath = cordova.file.dataDirectory; // // The path where the file will be created.
            
            // The name of your file, note that you need to know if is .png,.jpeg etc   
            var fileName = qId + '_' + (new Date()).toISOString().replace(/:/g, '-') + '.png';
            
            myUtilService.savebase64AsImageFile(folderpath, fileName, realData, dataType, 
                function () {
                    // Saving new or editing existing one?
                    if (!isEditing) signatureImageNameArr[qId].push(fileName);
                    else {
                        // If the editingSignatureImageUrl exists (user is editing the existing file), then replace the oldFileName with the new one and delete its old image file.
                        var oldFileName = editingSignatureImageUrl.substr(editingSignatureImageUrl.lastIndexOf('/') + 1);
                        var existingOneIndex = signatureImageNameArr[qId].indexOf(oldFileName);
                        if (existingOneIndex !== -1) {
                            var signatureImageNameArrByQuestionId = signatureImageNameArr[qId];
                            signatureImageNameArrByQuestionId[existingOneIndex] = fileName;
                        }
                    }
                    // Save DB first, then delete old file if it exists.
                    saveSignatureImageNameArrByQuestionIdToDb(qId, signatureImageNameArr[qId], 
                        function() {
                            if (isEditing) {
                                deleteSignatureImage(editingSignatureImageUrl, qId, 
                                    function() {
                                        successCb(fileName, signatureImageNameArr[qId]);
                                        console.log('save existing SignatureImage succesfully. Path is: ' + folderpath);
                                    }, 
                                    function() { console.log('Delete old signature image file FAILED.'); }
                                );
                            } else {
                                successCb();
                                console.log('save new signatureImage succesfully. Path is: ' + folderpath);
                            }                            
                        }
                    );
                }
            );
        }
        function deleteSignatureImage(imgPath, questionId, successCb, errorCb) {
            if (!imgPath || imgPath == '') return;
            var imageName = imgPath.substr(imgPath.lastIndexOf('/') + 1);

            window.resolveLocalFileSystemURL(cordova.file.dataDirectory, 
                function(dir) {
                    dir.getFile(imageName, {create:false}, 
                        function(fileEntry) {
                            fileEntry.remove(
                                function() { 
                                    console.log('deleteSignatureImage in device succesfully.'); 
                                    // Remove imageName in DB.
                                    var imageNameIndex = signatureImageNameArr[questionId].indexOf(imageName);
                                    if (imageNameIndex !== -1) signatureImageNameArr[questionId].splice(imageNameIndex, 1);
                                    saveSignatureImageNameArrByQuestionIdToDb(questionId, signatureImageNameArr[questionId], 
                                        function() {
                                            successCb();
                                        }
                                    );                                    
                                },
                                function(error) { 
                                    console.log('deleteSignatureImage failed. Error deleting the file.'); 
                                    errorCb(error);
                                },
                                function(error) { 
                                    console.log('deleteSignatureImage failed. The file does not exist.'); 
                                    errorCb(error);
                                }
                            );
                        },
                        function() {
                            // Though imageName does not exist, make sure imageName is deleted from DB.
                            console.log('The signatureImageName does not exist in device, but it is still being deleted from DB to make sure consistency.');                            
                            signatureImageNameArr[questionId].splice(signatureImageNameArr[questionId].indexOf(imageName), 1);
                            saveSignatureImageNameArrByQuestionIdToDb(questionId, signatureImageNameArr[questionId]);
                            errorCb(error);
                        }
                    );
                }
            );
        }

        return {
            getSignatureImageNameArrByQuestionId: getSignatureImageNameArrByQuestionId,
            saveSignatureImage: saveSignatureImage,
            deleteSignatureImage: deleteSignatureImage
        };
    }
})(angular.module('common.core'));