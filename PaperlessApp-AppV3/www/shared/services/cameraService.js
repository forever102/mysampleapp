/// <reference path="../../../.vscode/typings/angularjs/angular.d.ts"/>

(function (app) {
    app.factory('cameraService', cameraService);
    
    cameraService.$inject = ['$rootScope', '$cordovaCamera', '$q', '$cordovaFile', 'myUtilService', 'resultDetailRepo'];
    function cameraService($rootScope, $cordovaCamera, $q, $cordovaFile, myUtilService, resultDetailRepo) {
        /**
         * Initial stuffs.
         */
        var imagesNames = {};
        // var QUESTION_ID = 'images'; // This should be the questionId.

        /**
         * Helper methods.
         */
        function optionsForType(type) {
            var source;

            switch (type) {
                case 0:
                    source = Camera.PictureSourceType.CAMERA;
                    break;
                case 1:
                    source = Camera.PictureSourceType.PHOTOLIBRARY;
                    break;
            }

            return {
                quality: 100,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: source,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true
            };
        }

        function saveImagesByQuestionIdToDb(questionId, images, successCb) {            
            // To-do: This should be stored in DB by QUESTION_ID.
            // window.localStorage.setItem(questionId, JSON.stringify(images));
            // successCb();

            if (questionId && $rootScope.currentResultId) {
                var jsonParams = { 'questionId': questionId, 'answeredContent': JSON.stringify(images), 'resultId': $rootScope.currentResultId };
                resultDetailRepo.createOrReplace(jsonParams, function(tx, res) {
                    successCb();
                });
            }
        }

        /**
         * Main methods.
         */
        function getImagesNamesByQuestionId(questionId, successCb) {

            // To-do: This must be gotten from Sqlite Db by QUESTION_ID.
            // var imgs = window.localStorage.getItem(questionId);

            // if (imgs) imagesNames[questionId] = JSON.parse(imgs);
            // else {
            //     // Use array to allow users add multiple images for 1 question.
            //     imagesNames[questionId] = [];
            // }            
            // successCb(imagesNames[questionId]);
            
            resultDetailRepo.readByQuestionIdAndResultId(questionId, $rootScope.currentResultId, function(tx, res) {
                var item = res.rows.item(0);
                if (!item) {
                    // Use array to allow users add multiple images for 1 question.
                    imagesNames[questionId] = [];
                } else {
                    imagesNames[questionId] = JSON.parse(item.answeredContent);
                }
                console.info('questionId is: ' + questionId + ', imagesNames[questionId] is: ' + JSON.stringify(imagesNames[questionId]));
                successCb(imagesNames[questionId]);
            });
        };
        function addImageName(imgName, questionId) {
            imagesNames[questionId].push(imgName);

            if (imagesNames[questionId].length > 0) console.log('imagesNames[questionId] is: ' + JSON.stringify(imagesNames[questionId]));

            saveImagesByQuestionIdToDb(questionId, imagesNames[questionId], function() {});
        };
        function saveImage(type, questionId, finallyCb) {
            return $q(function () {
                var options = optionsForType(type);

                $cordovaCamera.getPicture(options).then(function (imagePath) {
                    // FileService.storeImage(imagePath);
                    // This saves image binary, not image's name. Should extract image's name and save image in data directory.
                    // console.log('imagePath is: ' + JSON.stringify(imagePath));

                    var name = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                    // newImageName is a combination of questionId_datetime_oldImageName.
                    var newName = questionId + '_' + (new Date()).toISOString().replace(/:/g, '-') + '_' + name;

                    //Move the file to permanent storage
                    $cordovaFile.moveFile(cordova.file.tempDirectory, name, cordova.file.dataDirectory, newName).then(function(success){
                        addImageName(newName, questionId);                        
                    }, function(error){ console.log('saveImage moveFile failed. Error is: ' + JSON.stringify(error)); });
                    finallyCb();
                });
            });
        }
        function updateImage(qId, base64String, editingImageUrl, finallyCb) {
            var block = base64String.split(';'); // Split the base64 string in data and contentType.
            var dataType = block[0].split(':')[1]; // Get the content type. // In this case "image/png".
            var realData = block[1].split(",")[1]; // Get the real base64 content of the file. In this case "iVBORw0KGg....".
            var folderpath = cordova.file.dataDirectory; // // The path where the file will be created.

            // The name of your file, note that you need to know if is .png,.jpeg etc   
            var fileName = qId + '_' + (new Date()).toISOString().replace(/:/g, '-') + '.png';

            myUtilService.savebase64AsImageFile(folderpath, fileName, realData, dataType, 
                function () {
                    // Replace the oldFileName with the new one and delete its old image file.
                    var oldFileName = editingImageUrl.substr(editingImageUrl.lastIndexOf('/') + 1);
                    var existingOneIndex = imagesNames[qId].indexOf(oldFileName);
                    if (existingOneIndex !== -1) {
                        var imageNameArrByQuestionId = imagesNames[qId];
                        imageNameArrByQuestionId[existingOneIndex] = fileName;
                    }
                    // Save DB first, then delete old file if it exists.
                    saveImagesByQuestionIdToDb(qId, imagesNames[qId], 
                        function() {
                            deleteImage(editingImageUrl, qId, 
                                function() {
                                    finallyCb(fileName, imagesNames[qId]);
                                    console.info('save existing image succesfully. Path is: ' + folderpath);
                                }, 
                                function() { 
                                    console.error('Delete old image file FAILED.'); 
                                    finallyCb();
                                }
                            );
                        }
                    );
                }
            );
        }
        function deleteImage(imgPath, questionId, successCb, errorCb) {
            var imageName = imgPath.substr(imgPath.lastIndexOf('/') + 1);

            window.resolveLocalFileSystemURL(cordova.file.dataDirectory, 
                function(dir) {
                    dir.getFile(imageName, {create:false}, 
                        function(fileEntry) {
                            fileEntry.remove(
                                function() { 
                                    console.info('deleteImage in device succesfully.'); 
                                    // Remove imageName in DB.
                                    var imageNameIndex = imagesNames[questionId].indexOf(imageName);
                                    if (imageNameIndex !== -1) imagesNames[questionId].splice(imagesNames[questionId].indexOf(imageName), 1);
                                    saveImagesByQuestionIdToDb(questionId, imagesNames[questionId], function() {});
                                    successCb();
                                },
                                function(error) { 
                                    console.error('deleteImage failed. Error deleting the file.'); 
                                    errorCb(error);
                                },
                                function(error) { 
                                    console.log('deleteImage failed. The file does not exist.'); 
                                    errorCb(error);
                                }
                            );
                        },
                        function() {
                            // Though imageName does not exist, make sure imageName is deleted from DB.
                            console.log('The imageName does not exist in device, but it is still being deleted from DB to make sure consistency.');                            
                            imagesNames[questionId].splice(imagesNames[questionId].indexOf(imageName), 1);
                            saveImagesByQuestionIdToDb(questionId, imagesNames[questionId], function() {});
                            errorCb(error);
                        }
                    );
                }
            );
        }        

        return {
            getImagesNamesByQuestionId: getImagesNamesByQuestionId,
            addImageName: addImageName,
            saveImage: saveImage,
            updateImage: updateImage,
            deleteImage: deleteImage
        };
    }
})(angular.module('common.core'));