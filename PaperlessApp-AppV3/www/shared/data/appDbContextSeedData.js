(function (app) {
    app.factory('appDbContextSeedData', appDbContextSeedData);

    appDbContextSeedData.$inject = ['$http', 'appDbContext'];
    function appDbContextSeedData($http, appDbContext) {
        var self = this;
                        
        var baseUrl = 'shared/data/sampleData';
        return {
            seedSampleData: seedSampleData
        };

        function seedSampleData(successCb, errorCb) {
            appDbContext.initDb(function (db) {
                self.db = db;
                // Seed sample data if there's no any row in each table.
                checkAnyRowInEachTable(function (tx, res) {
                    // Must be 6 as DB has 6 tables.
                    // console.log('rowcount is: ' + res.rows.item(0).rowcount);
                    if (res.rows.length > 0 && res.rows.item(0).rowcount == 6) {
                        console.info('Tables and rows already exist.');
                        successCb();
                    } else {
                        console.info('Tables and rows NOT exist. Seeding sample data is executed.');
                        // Get questionTypes first, then getHardWareTypes (terminals) and getQuestions.
                        $http.get(baseUrl + '/getQuestionTypes.json').then(
                            function (getQuestionTypesResponse) {
                                $http.get(baseUrl + '/getHardWareTypes.json').then(
                                    function (getHardWareTypesResponse) {
                                        $http.get(baseUrl + '/getQuestions.json').then(
                                            function (getQuestionsResponse) {
                                                // Wrap everything in transaction.
                                                self.db.transaction(
                                                    function (tx) {
                                                        // Seed questionTypes and terminals first.
                                                        // console.log('questionTypes are: ');
                                                        getQuestionTypesResponse.data.forEach(function (value, index) {
                                                            // console.log('id is: ' + value.id + ', questionType is: ' + value.questionType);
                                                            tx.executeSql('Insert Or Replace Into QuestionTypes(Id, QuestionTypeName) Values (?,?)', [value.ID, value.QuestionType],
                                                                function (tx, res) {
                                                                    // console.log('insertId: ' + res.insertId + ' -- probably 1' + ', rowsAffected: ' + res.rowsAffected + ' -- should be 1');
                                                                },
                                                                function (tx, error) { console.error('Seed questionTypes failed. Error is: ' + JSON.stringify(error)); }
                                                            );
                                                        });

                                                        getHardWareTypesResponse.data.forEach(function (value, index) {
                                                            tx.executeSql('Insert Or Replace Into Terminals (Id, Name) Values (?, ?)', [value.ID, value.Details],
                                                                function (tx, res) { },
                                                                function (tx, error) { console.error('Seed terminals failed. Error is: ' + JSON.stringify(error)); }
                                                            );
                                                        });

                                                        // Then, seed questionnaires, terminals, QuestionairHeaders (sections) and questions.
                                                        // Group data using underscore nest library.
                                                        getQuestionsResponse.data = _.nest(getQuestionsResponse.data, ['QuestionairName', 'QuestionTerminalType', 'QuestionHeader']);
                                                        var questionnaires = getQuestionsResponse.data.children;
                                                        // console.log(JSON.stringify(getQuestionsResponse.data));
                                                        // Insert QuestionaireName (questionnaires).
                                                        // console.log('questionnaires are: ');
                                                        questionnaires.forEach(function (questionnaireValue, questionnaireIndex) {
                                                            // console.log('questionnaireValue is: ' + questionnaireValue.name + ', questionnaireIndex is: ' + questionnaireIndex);
                                                            tx.executeSql('Insert Or Replace Into Questionnaires (Id, Name) Values ((select Id from Questionnaires where Name = ?), ?)', [questionnaireValue.name, questionnaireValue.name],
                                                                function (tx, res1) {
                                                                    var questionnaireInsertId = res1.insertId;
                                                                    var terminals = questionnaireValue.children;
                                                                    terminals.forEach(function (t, tIndex) {
                                                                        // console.log('t is: ' + t.name + ', tIndex is: ' + tIndex);
                                                                        var terminalId = t.name;
                                                                        // Insert QuestionnaireTerminals (linking table between Questionnaires and Terminals tables).
                                                                        // console.log('questionnaireInsertId is: ' + questionnaireInsertId + ', terminalId is: ' + terminalId);
                                                                        var valuesArr = [questionnaireInsertId, terminalId, questionnaireInsertId, terminalId];
                                                                        tx.executeSql('Insert Or Replace Into QuestionnaireTerminals (Id, QuestionnaireId, TerminalId) Values ((select Id from QuestionnaireTerminals where QuestionnaireId = ? And TerminalId = ?), ?, ?)', valuesArr,
                                                                            function (tx, res3) { 
                                                                                var questionnaireTerminalId = res3.insertId;
                                                                                // Insert QuestionHeader (Sections).
                                                                                var sections = t.children;
                                                                                sections.forEach(function (s, sIndex) {
                                                                                    var sValuesArr = [s.name, s.name, questionnaireTerminalId];
                                                                                    tx.executeSql('Insert Or Replace Into Sections (Id, Name, QuestionnaireTerminalId) Values ((Select Id From Sections Where Name = ?), ?, ?)', sValuesArr,
                                                                                        function (tx, res4) {
                                                                                            var sectionInsertId = res4.insertId;
                                                                                            // Insert Questions.
                                                                                            var questions = s.children;
                                                                                            questions.forEach(function (question, questionIndex) {
                                                                                                var qValuesArr = [question.QuestionID, question.QuestionText, question.QuestionSort, question.QuestionType, sectionInsertId];
                                                                                                tx.executeSql('Insert Or Replace Into Questions (Id, Content, [Order], QuestionTypeId, SectionId) Values (?, ?, ?, ?, ?)', qValuesArr,
                                                                                                    function (tx, res5) { },
                                                                                                    function (tx, err) { console.error('Seed Questions failed. Error is: ' + JSON.stringify(err)); }
                                                                                                );
                                                                                            });
                                                                                        },
                                                                                        function (tx, err) { console.error('Seed Sections failed. Error is: ' + JSON.stringify(err)); }
                                                                                    );
                                                                                });
                                                                            },
                                                                            function (tx, err) { console.error('Seed QuestionnaireTerminals failed. Error is: ' + JSON.stringify(err)); }
                                                                        );                                                                        
                                                                    });
                                                                },
                                                                function (tx, err) { console.error('Seed questionnaires failed. Error is: ' + JSON.stringify(err)); }
                                                            );
                                                        });
                                                    },
                                                    function (error) {
                                                        console.error('seedSampleData Transaction error: ' + JSON.stringify(error));
                                                        errorCb(error);
                                                    },
                                                    function () {
                                                        console.info('seedSampleData Transaction ok.');
                                                        successCb();
                                                        // Check results.
                                                    }
                                                );
                                            },
                                            function (err) {
                                                console.error('getQuestionsResponse failed. Error is: ' + JSON.stringify(err));
                                                errorCb(error);
                                            }
                                        );
                                    },
                                    function (error) {
                                        console.error('getHardWareTypes failed. Error is: ' + JSON.stringify(error));
                                        errorCb(error);
                                    }
                                );
                            },
                            function (error) {
                                console.error('getQuestionTypesResponse failed. Error is: ' + JSON.stringify(error));
                                errorCb(error);
                            }
                        );
                    }
                });
            });
        }

        // Helper methods.
        function checkAnyRowInEachTable(successCb) {
            var query = `
                select sum(rowcount) as rowcount
                    from (
                        select EXISTS(SELECT 1 FROM QuestionTypes LIMIT 1) as rowcount
                        union all
                        select EXISTS(SELECT 1 FROM Questionnaires LIMIT 1) as rowcount
                        union all
                        select EXISTS(SELECT 1 FROM Terminals LIMIT 1) as rowcount
                        union all
                        select EXISTS(SELECT 1 FROM QuestionnaireTerminals LIMIT 1) as rowcount
                        union all
                        select EXISTS(SELECT 1 FROM Sections LIMIT 1) as rowcount
                        union all
                        select EXISTS(SELECT 1 FROM Questions LIMIT 1) as rowcount
                    ) as rowcounts;
            `;
            var valueParams = [];
            appDbContext.doExecuteQuery(self.db, query, valueParams,
                function (tx, res) { successCb(tx, res); },
                function (tx, err) { console.error('checkAnyRowInEachTable failed. Error is: ' + JSON.stringify(err)) }
            );
        }
    }
})(angular.module('common.core'));