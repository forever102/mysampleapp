(function (app) {
    app.factory('appDbContext', appDbContext);

    appDbContext.$inject = ['$cordovaSQLite', '$ionicPlatform', '$http'];
    function appDbContext($cordovaSQLite, $ionicPlatform, $http) {
        var self = this;
        self.db = null;
        // Use the prepopulated DB from EF-Core and rename the db based on its device OS.
        self.dbName = 'PaperlessApp.db';

        function initDb(successCb) {
            $ionicPlatform.ready(function () {                
                if (self.db) successCb(self.db);
                else {
                    // Use the prepopulated DB.
                    self.db = window.sqlitePlugin.openDatabase(
                        {
                            name: self.dbName,
                            location: 'default',
                            createFromLocation: 1
                        }
                    );

                    if (self.db) {
                        console.info('Open DB successfully. Path of db is: ' + getDbPath());
                        successCb(self.db);
                    } else console.error('Open DB failed.');
                }                
            });
        }

        function dropDb() {
            $ionicPlatform.ready(function () {
                window.sqlitePlugin.deleteDatabase({
                    name: self.dbName,
                    location: 'default'
                }, 
                function (res) { console.info('Drop existing DB successfully.'); },
                function (err) { console.error('Drop existing DB failed. Error is: ' + JSON.stringify(err)); });
            });
        }

        function syncDataWithServer(jsonDataFromServer, successCb) {
            initDb(function (res) {
                // Once jsonData is completely available (from Server), parse and sync with sample data in sqlite db.
                console.info('sync data with server in progress... (not implemented yet)');
                successCb();
            });            
        }        

        // Helper methods.
        function doExecuteQuery(db, query, dataArray, successCb, errorCb) {
            $ionicPlatform.ready(function () {
                // Fixing issue of Null db while using Live Reload feature.
                if (!db) initDb(function (notNullDb) { db = notNullDb; });
                db.transaction(
                    function (tx) {
                        tx.executeSql(query, dataArray, 
                            function (tx, res) { successCb(tx, res); }, 
                            function (tx, err) { errorCb(tx, err); }
                        );
                    },
                    function (err) { console.error('doExecuteQuery transaction error: ' + JSON.stringify(err)); },
                    function () { console.info('doExecuteQuery transaction Ok.'); }
                );
            });
        }

        function getDbPath() {
            var path = '';
            if (ionic.Platform.isIOS()) path = cordova.file.applicationStorageDirectory + 'Library/LocalDatabase';
            else if (ionic.Platform.isAndroid()) path = cordova.file.applicationStorageDirectory + 'databases';
            else if (ionic.Platform.isWindowsPhone()) path = 'Unknown path of WindowsPhone';
            else path = 'Unknown path of unknownOs';            
            return path;
        }

        /**
         * Starting point.
         */
        return {
            dropDb: dropDb,
            initDb: initDb,
            syncDataWithServer: syncDataWithServer,
            doExecuteQuery: doExecuteQuery
        };
    }
})(angular.module('common.core'));