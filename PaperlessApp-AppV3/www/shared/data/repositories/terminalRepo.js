(function (app) {
    app.factory('terminalRepo', terminalRepo);

    terminalRepo.$inject = ['$q', 'appDbContext'];
    function terminalRepo($q, appDbContext) {
        var self = this;
        self.db = null; 
        if (!self.db) appDbContext.initDb(function (db) { self.db = db; });

        return {
            create: create,
            readAll: readAll,
            readByQuestionnaireId: readByQuestionnaireId,
            updateById: updateById,
            deleteById: deleteById
        };

        function create() {}
        function readAll(successCb) {}
        function readByQuestionnaireId(questionnaireId) {
            return $q(function(resolve, reject) {
                var query = `
                    SELECT T.Id as id, T.Name as name 
                        FROM Terminals as T 
                        Left Join QuestionnaireTerminals QT on T.Id = QT.TerminalId 
                        Where T.Id = QT.TerminalId And QT.QuestionnaireId = ?
                `;
                appDbContext.doExecuteQuery(self.db, query, [questionnaireId],
                    function (tx, res) {
                        resolve(res);
                    },
                    function (tx, err) { 
                        console.error('readByQuestionnaireId Terminals failed. Error is: ' + JSON.stringify(err));
                        reject(err);
                    }
                );
            });
        }
        function updateById() {}
        function deleteById() {}
    }
})(angular.module('common.core'));