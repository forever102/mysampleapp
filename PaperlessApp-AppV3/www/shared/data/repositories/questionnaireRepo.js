(function (app) {
    app.factory('questionnaireRepo', questionnaireRepo);

    questionnaireRepo.$inject = ['$q', 'appDbContext'];
    function questionnaireRepo($q, appDbContext) {
        var self = this;
        appDbContext.initDb(function (db) { self.db = db; });

        return {
            create: create,
            readAll: readAll,
            readById: readById,
            updateById: updateById,
            deleteById: deleteById
        };

        function create() {}
        function readAll() {
            return $q(function(resolve, reject) {
                var query = 'SELECT Id as id, Name as name FROM Questionnaires';
                appDbContext.doExecuteQuery(self.db, query, [],
                    function (tx, res) {
                        resolve(res);
                    },
                    function (tx, err) { 
                        console.error('readAll Questionnaires failed. Error is: ' + JSON.stringify(err)); 
                        reject(err);
                    }
                );
            });            
        }
        function readById() {}
        function updateById() {}
        function deleteById() {}
    }
})(angular.module('common.core'));