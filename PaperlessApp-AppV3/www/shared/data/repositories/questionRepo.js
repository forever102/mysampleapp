(function (app) {
    app.factory('questionRepo', questionRepo);

    questionRepo.$inject = ['$q', 'appDbContext'];
    function questionRepo($q, appDbContext) {
        var self = this;
        self.db = null;
        if (!self.db) appDbContext.initDb(function (db) { self.db = db; });        

        function readByResultIdAndSectionId(resultId, sectionId) {
            if (resultId && sectionId) {
                return $q(function(resolve, reject) {
                    var query = `
                        SELECT Q.Id As id, Q.Content As content, (Select AnsweredContent From ResultDetails Where QuestionId = Q.Id And ResultId = ?) as answer, Q.[Order] As [order], QT.QuestionTypeName As questionTypeName, Q.QuestionTypeId as questionTypeId
                            FROM Questions As Q
                            Left Join QuestionTypes As QT On Q.QuestionTypeId = QT.Id
                            Where Q.QuestionTypeId = QT.Id And SectionId = ?
                            Order By Q.[Order] ASC
                    `;
                    appDbContext.doExecuteQuery(self.db, query, [resultId, sectionId],
                        function (tx, res) { 
                            resolve(res);
                            // successCb (tx, res);
                        },
                        function (tx, err) { 
                            console.error('questionRepo readByResultIdAndSectionId failed. Error is: ' + JSON.stringify(err));                             
                            reject(err);
                        }
                    );
                });
            }
        }

        return {
            create: null,
            readByResultIdAndSectionId: readByResultIdAndSectionId,
            updateById: null,
            deleteById: null
        };
    }
})(angular.module('common.core'));