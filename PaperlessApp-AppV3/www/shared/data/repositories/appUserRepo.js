(function (app) {
    app.factory('appUserRepo', appUserRepo);

    appUserRepo.$inject = ['appDbContext'];
    function appUserRepo(appDbContext) {
        var self = this;
        
        appDbContext.initDb(function (db) { self.db = db; });        

        function create(newUser, successCb) {
            if (newUser.userName && newUser.userName != '') {
                var valuesArr = [newUser.userName, newUser.userName];
                // Insert if not exist, else, update.
                var query = "Insert Or Replace Into AppUsers (Id, UserName) Values ((Select Id From AppUsers Where UserName = ?), ?)";
                appDbContext.doExecuteQuery(self.db, query, valuesArr, 
                    function(tx, res) {
                        successCb(tx, res);
                    },
                    function (tx, err) { console.error('appUserRepo create failed. Error is: ' + JSON.stringify(err)); }
                );
            }            
        }
        function readAll(successCb) {
        }
        function readById() {}
        function updateById() {}
        function deleteById() {}

        return {
            create: create,
            readAll: readAll,
            readById: readById,
            updateById: updateById,
            deleteById: deleteById
        };
    }
})(angular.module('common.core'));