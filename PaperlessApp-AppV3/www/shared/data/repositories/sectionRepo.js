(function (app) {
    app.factory('sectionRepo', sectionRepo);

    sectionRepo.$inject = ['$q', 'appDbContext'];
    function sectionRepo($q, appDbContext) {
        var self = this;
        self.db = null;
        if (!self.db) appDbContext.initDb(function (db) { self.db = db; });        

        // function readByQuestionnaireIdAndTerminalId(questionnaireId, terminalId, successCb) {
        //     var query = `
        //         SELECT Id as id, Name as name FROM Sections 
	    //             Where QuestionnaireTerminalId = (Select Id From QuestionnaireTerminals Where QuestionnaireId = ? And TerminalId = ?)
        //     `;
        //     appDbContext.doExecuteQuery(self.db, query, [questionnaireId, terminalId], 
        //         function (tx, res) { successCb (tx, res) },
        //         function (tx, err) { console.error('readByQuestionnaireIdAndTerminalId failed. Error is: ' + JSON.stringify(err)) }
        //     );
        // }

        function readByResultId(resultId) {
            return $q(function(resolve, reject) {
                var query = `
                    SELECT S.Id as id, S.Name as name,
                        (SELECT Count(*) FROM Questions As Q2, QuestionTypes As QT2 Where Q2.QuestionTypeId = QT2.Id And SectionId = S.Id) as totalQuestions,
                        (Select countValue FROM ((SELECT Count(*) as countValue, (Select AnsweredContent From ResultDetails Where QuestionId = Q3.Id And ResultId = ?) as answer
                            FROM Questions As Q3, QuestionTypes As QT3
                            Where Q3.QuestionTypeId = QT3.Id And SectionId = S.Id And ( answer is null or answer = '' or answer = '[]' or answer = 'false')))) as numberOfEmptyAnswer
                    FROM Sections As S, Questions As Q
                        Left Join QuestionTypes As QT On Q.QuestionTypeId = QT.Id
                        Where S.QuestionnaireTerminalId = (Select QuestionnaireTerminalId From Results Where Id = ?)
                        Group By S.Id
                `;
                appDbContext.doExecuteQuery(self.db, query, [resultId, resultId], 
                    function (tx, res) { resolve(res); },
                    function (tx, err) { 
                        console.error('readByResultId failed. Error is: ' + JSON.stringify(err)) 
                        reject(err);
                    }
                );
            });            
        }

        return {
            create: null,
            readAll: null,
            // readByTerminalId: readByTerminalId,
            // readByQuestionnaireIdAndTerminalId: readByQuestionnaireIdAndTerminalId,
            readByResultId: readByResultId,
            updateById: null,
            deleteById: null
        };
    }
})(angular.module('common.core'));