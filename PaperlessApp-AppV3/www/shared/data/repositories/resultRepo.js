(function (app) {
    app.factory('resultRepo', resultRepo);

    resultRepo.$inject = ['$q', 'appDbContext'];
    function resultRepo($q, appDbContext) {
        var self = this;
        
        appDbContext.initDb(function (db) { self.db = db; });        

        function create(jsonParams, successCb) {
            if (jsonParams.userName && jsonParams.questionnaireId && jsonParams.terminalId) {
                var valuesArr = [jsonParams.userName, jsonParams.questionnaireId, jsonParams.terminalId];
                var query = `
                    Insert Into Results (AppUserId, QuestionnaireTerminalId) 
                    	Values ((Select Id From AppUsers Where UserName = ?), (Select Id From QuestionnaireTerminals Where QuestionnaireId = ? And TerminalId = ?))
                `;
                appDbContext.doExecuteQuery(self.db, query, valuesArr, 
                    function(tx, res) {
                        successCb(res.insertId);
                        // // Create a record resultDetails with this result.
                        // var resultInsertId = res.insertId;
                        // tx.executeSql('Insert Into ResultDetails (QuestionId, ResultId) Values (0, ?)', [resultInsertId],
                        //     function (tx, res2) { 
                        //         readByIdAndQuestionnaireAndTerminalId(jsonParams.userName, jsonParams.questionnaireId, jsonParams.terminalId, resultInsertId, function(res3) {
                        //             successCb(res3);
                        //         })
                        //     },
                        //     function (tx, err) { console.error('resultRepo create ResultDetails failed. Error is: ' + JSON.stringify(err)); }
                        // );
                    },
                    function (tx, err) { console.error('resultRepo create failed. Error is: ' + JSON.stringify(err)); }
                );
            }
        }
        function readAll(successCb) {
        }
        function readById(resultId) { }
        function readByIdAndQuestionnaireAndTerminalId(userName, qId, tId, id, successCb) {
            var valuesArr = [userName, qId, tId, id];
            var query = `
                Select Id as id, AppUserId as appUserId, CreatedDate as createdDate, QuestionnaireTerminalId as questionnaireTerminalId, UpdatedDate as updatedDate, isComplete 
                    From Results 
                    Where AppUserId = (Select Id From AppUsers Where UserName = ?) And QuestionnaireTerminalId = (Select Id From QuestionnaireTerminals Where QuestionnaireId = ? And TerminalId = ?) And Id = ? Limit 1
            `;
            appDbContext.doExecuteQuery(self.db, query, valuesArr, 
                function(tx, res) {
                    successCb(res);
                },
                function (tx, err) { 
                    console.error('resultRepo readByIdAndQuestionnaireAndTerminalId failed. Error is: ' + JSON.stringify(err)); 
                }
            );
        }
        // function readByQuestionnaireIdAndTerminalId(jsonParams, successCb) {
        //     if (jsonParams.userName && jsonParams.questionnaireId && jsonParams.terminalId) {
        //         var valuesArr = [jsonParams.userName, jsonParams.questionnaireId, jsonParams.terminalId];
        //         var query = `
        //             Select Id as id, AppUserId as appUserId, CreatedDate as createdDate, QuestionnaireTerminalId as questionnaireTerminalId, UpdatedDate as updatedDate, isComplete 
	    //                 From Results 
	    //                 Where AppUserId = (Select Id From AppUsers Where UserName = ?) And QuestionnaireTerminalId = (Select Id From QuestionnaireTerminals Where QuestionnaireId = ? And TerminalId = ?)
        //         `;
        //         appDbContext.doExecuteQuery(self.db, query, valuesArr, 
        //             function(tx, res) {
        //                 successCb(tx, res);
        //             },
        //             function (tx, err) { console.error('resultRepo readByQuestionnaireIdAndTerminalId failed. Error is: ' + JSON.stringify(err)); }
        //         );
        //     }
        // }
        function readByQuestionnaireAndTerminalId(jsonParams) {
            if (jsonParams.userName && jsonParams.questionnaireId && jsonParams.terminalId) {
                return $q(function (resolve, reject) {
                    var valuesArr = [jsonParams.userName, jsonParams.questionnaireId, jsonParams.terminalId];
                    var query = `
                        Select Id as id, AppUserId as appUserId, CreatedDate as createdDate, QuestionnaireTerminalId as questionnaireTerminalId, UpdatedDate as updatedDate, isComplete 
                            From Results 
                            Where AppUserId = (Select Id From AppUsers Where UserName = ?) And QuestionnaireTerminalId = (Select Id From QuestionnaireTerminals Where QuestionnaireId = ? And TerminalId = ?)
                    `;
                    appDbContext.doExecuteQuery(self.db, query, valuesArr, 
                        function(tx, res) {
                            resolve(res);
                        },
                        function (tx, err) { 
                            console.error('resultRepo readByQuestionnaireIdAndTerminalId failed. Error is: ' + JSON.stringify(err)); 
                            reject(err);
                        }
                    );
                });
            }
        }
        function updateById() {}
        function deleteById() {}

        return {
            create: create,
            readAll: readAll,
            readById: readById,
            readByIdAndQuestionnaireAndTerminalId: readByIdAndQuestionnaireAndTerminalId,
            // readByQuestionnaireIdAndTerminalId: readByQuestionnaireIdAndTerminalId,
            readByQuestionnaireAndTerminalId: readByQuestionnaireAndTerminalId,
            updateById: updateById,
            deleteById: deleteById
        };
    }
})(angular.module('common.core'));