(function (app) {
    app.factory('resultDetailRepo', resultDetailRepo);

    resultDetailRepo.$inject = ['appDbContext'];
    function resultDetailRepo(appDbContext) {
        var self = this;
        self.db = null; 
        if (!self.db) appDbContext.initDb(function (db) { self.db = db; });        

        function createOrReplace(jsonParams, successCb) {
            if (jsonParams.questionId && jsonParams.answeredContent && jsonParams.resultId) {
                var valuesArr = [jsonParams.questionId, jsonParams.resultId, jsonParams.questionId, jsonParams.answeredContent, jsonParams.resultId, jsonParams.questionId, jsonParams.resultId];
                var query = `
                    Insert Or Replace Into ResultDetails (Id, QuestionId, AnsweredContent, ResultId, CreatedDate) 
                    	Values ((Select Id From ResultDetails Where QuestionId = ? And ResultId = ?), ?, ?, ?, (Select CreatedDate From ResultDetails Where QuestionId = ? And ResultId = ?))
                `;
                appDbContext.doExecuteQuery(self.db, query, valuesArr, 
                    function(tx, res) {
                        var currentDate = (new Date()).toISOString();
                        // As the answer is changed, we need to update the UpdatedDate of Results table.
                        tx.executeSql('Update Results Set UpdatedDate = ? Where Id = ?', [currentDate, jsonParams.resultId],
                            function (tx, res2) {                                 
                                successCb(tx, res);      
                                console.info('resultDetailRepo createOrReplace successfully.');
                            },
                            function (tx, err) { console.error('resultDetailRepo createOrReplace ResultDetails failed. Error is: ' + JSON.stringify(err)); }
                        );                        
                    },
                    function (tx, err) { console.error('resultDetailRepo createOrReplace failed. Error is: ' + JSON.stringify(err)); }
                );
            }
        }
        function readByQuestionIdAndResultId(qId, resultId, successCb) {
            if (qId && resultId) {
                var valuesArr = [qId, resultId];
                var query = `
                    Select AnsweredContent As answeredContent From ResultDetails
                        Where QuestionId = ? And ResultId = ? Limit 1
                `;
                appDbContext.doExecuteQuery(self.db, query, valuesArr, 
                    function(tx, res) {
                        successCb(tx, res);
                    },
                    function (tx, err) { console.error('resultDetailRepo readByQuestionIdAndResultId failed. Error is: ' + JSON.stringify(err)); }
                );
            }
        }
        function readAll(successCb) {}        
        function updateById() {}
        function deleteById() {}

        return {
            createOrReplace: createOrReplace,
            readByQuestionIdAndResultId: readByQuestionIdAndResultId,
            readAll: readAll,
            updateById: updateById,
            deleteById: deleteById
        };
    }
})(angular.module('common.core'));