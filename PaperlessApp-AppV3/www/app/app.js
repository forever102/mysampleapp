(function () {
    'use strict';
    
    angular.module('common.core', ['ngCordova']);
    angular.module('common.ui', ['ngAnimate', 'toastr']);
    angular.module('paperlessAppV3', ['ionic', 'common.core', 'common.ui']).config(configuration).run(run);

    configuration.$inject = ['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider', 'toastrConfig'];
    function configuration($stateProvider, $urlRouterProvider, $ionicConfigProvider, toastrConfig) {        
        // Config toastr service.
        angular.extend(toastrConfig, {
            autoDismiss: false,
            containerId: 'toast-container',
            maxOpened: 0,
            newestOnTop: true,
            positionClass: 'toast-top-right',
            preventDuplicates: false,
            preventOpenDuplicates: false,
            target: 'body'
        });

        // Setup routes. // Disable cach-view as result needs to be up-to-date with Sqlite Db.
        $stateProvider
            .state('tab', {
                url: '/tab',
                abstract: true,
                template: `<tabs></tabs>`
            })
            // Each tab has its own nav history stack:
            .state('tab.login', {
                url: '/login',
                views: {
                    'tab-login': {
                        template: `<login></login>`
                    }
                }
            })
            .state('tab.about', {
                url: '/about',
                views: {
                    'tab-about': {
                        template: `<about></about>`,
                    }
                }
            })

            /**
             * - Setup sideMenu navigation stacks.
             * - NOTE Here: I don't use component template for 'home' (abstract state) as Ionic 1 routing doesn't support it.
             */
            .state('home', {
                url: '/home',
                abstract: true,
                templateUrl: 'components/home/home.html',
                controller: 'homeCtrl as $ctrl',
                resolve: { isAuthenticated: isAuthenticated } 
            })
            .state('home.questionnaires', {
                url: '/questionnaires',
                views: {
                    'menuContent': {                        
                        template: `
                            <ion-view view-title="Questionnaires">
                                <ion-content class="has-header" padding="true"><questionnaires></questionnaires></ion-content>
                            </ion-view>
                        `
                    }
                }
            })
            .state('home.terminals', {
                url: '/questionnaires/:questionnaireId',
                views: {
                    'menuContent': {
                        template: `
                            <ion-view view-title="Terminals">
                                <ion-content class="has-header" padding="true"><terminals></terminals></ion-content>
                            </ion-view>
                        `
                    }
                }
            })
            .state('home.results', {
                url: '/questionnaires/:questionnaireId/terminalId/:terminalId',
                views: {
                    'menuContent': {                        
                        template: `
                            <ion-view view-title="Results">
                                <ion-content class="has-header" padding="true"><results></results></ion-content>
                            </ion-view>
                        `
                    }
                }
            })
            .state('home.sections', {
                url: '/questionnaires/:questionnaireId/terminals/:terminalId/resultId/:resultId',
                views: {
                    'menuContent': {
                        template: `
                            <ion-view view-title="Sections">
                                <ion-nav-buttons side="right">
                                    <button class="button" ng-click="">Submit</button>
                                </ion-nav-buttons>
                                <ion-content class="has-header" padding="true"><sections></sections></ion-content>
                            </ion-view>
                        `
                    }
                }
            })
            .state('home.questions', {
                url: '/:questionnaireId/:sectionId/:sectionIndex/:terminalId/resultId/:resultId',
                views: {
                    'menuContent': {
                        template: `
                            <ion-view view-title="Questions">
                                <ion-nav-buttons side="right">
                                    <button class="button" ng-click="">Save</button>
                                </ion-nav-buttons>
                                <ion-content class="has-header" padding="true"><questions></questions></ion-content>
                            </ion-view>
                        `
                    }
                }
            })
            .state('home.profile', {
                url: '/profile',
                views: {
                    'menuContent': {
                        template: `<ion-view view-title="Profile"><ion-content class="has-header" padding="true"><profile></profile></ion-content></ion-view>`
                    }
                }
            });

        $urlRouterProvider.otherwise('/home/questionnaires');
    }

    run.$inject = ['$ionicPlatform', 'appDbContext', '$http'];
    function run($ionicPlatform, appDbContext, $http) {
        // handle page refreshes.
        var loggedUser = window.localStorage.getItem('loggedUser') || {};
        if (loggedUser) {
            $http.defaults.headers.common['Authorization'] = loggedUser.authdata;
        }

        // Init or drop (if you want to refresh) Database.
        appDbContext.initDb(function(db) {
            console.info('initDb successfully.');          
        });
        // appDbContext.dropDb();

        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    }

    /**
     * Authenticating user.
     */
    isAuthenticated.$inject = ['$rootScope', 'authService', '$location'];
    function isAuthenticated($rootScope, authService, $location) {
        var isAuthenticated = authService.isUserLoggedIn();
        console.info('isAuthenticated is: ' + isAuthenticated);
        if (!isAuthenticated) {
            $rootScope.previousState = $location.path();
            $location.path('/tab/login');
        }
    }
})();