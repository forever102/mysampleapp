/// <reference path="../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.component('login', {
        templateUrl: 'components/login/login.html',
        bindings: {
            text: '='
        },
        controller: loginCtrl
    });

    loginCtrl.$inject = ['$rootScope', '$state', '$ionicLoading', 'authService', '$location', 'notificationService', 'appDbContextSeedData', 'appUserRepo'];
    function loginCtrl($rootScope, $state, $ionicLoading, authService, $location, notificationService, appDbContextSeedData, appUserRepo) {
        var $ctrl = this;

        $ctrl.doLogin = function () {
            // console.info('$ctrl.user is: ' + JSON.stringify($ctrl.user));
            
            $ionicLoading.show({
                template: `<p>Logging in...</p><ion-spinner></ion-spinner>`
            }).then(function(){
                authService.login($ctrl.user, loginCompletedCb, loginFinallyCb)                 
            });
        };

        /**
         * Helper functions.
         */
        function loginCompletedCb(result) {            
            var authenticatedUser = null;            
            if (result.data.indexOf('1') > -1 && result.data.indexOf('2') > -1) {
                result.data = result.data.replace(/\t/g, '');
                var token = result.data.substring(result.data.indexOf('2') + 1);
                authenticatedUser = { "token": token, "userName": $ctrl.user.userName, "role": "1" }
            }
            if (authenticatedUser) {
                // Save authenticatedUser (not include password and token) into Sqlite table. Token is stored in secured cached.
                appUserRepo.create(authenticatedUser, function(tx, res) {
                    // Seed sample data if user is authenticated and inserted successfully.
                    appDbContextSeedData.seedSampleData(
                        function (res) {
                            authService.saveCredentials(authenticatedUser);
                            notificationService.displaySuccess('Hello ' + authenticatedUser.userName);
                            // $ctrl.userData.displayUserInfo();
                            if ($rootScope.previousState)
                                $location.path($rootScope.previousState);
                            else {
                                $location.path('/home/questionnaires');
                            }
                        },
                        function (error) { 
                            console.log('appDbContextSeedData.seedSampleData failed. Error is: ' + JSON.stringify(error)); 
                        }
                    );                     
                });
            }
            else {
                notificationService.displayError('Login failed. Try again.');
            }
        }
        function loginFinallyCb() {
            $ionicLoading.hide().then(function(){
                // console.log("The loading indicator is now hidden");
            });
        }

        /**
         * Starting point.
         */
        $ctrl.$onInit = function() {
            // Pre-fill credentials for faster testing.
            $ctrl.user = {};
            $ctrl.user.userName = 'This is secret. Sorry';
            $ctrl.user.password = 'This is secret. Sorry';
        };
    }
})(angular.module('common.core'));