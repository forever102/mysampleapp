(function (app) {
    app.component('profile', {
        templateUrl: 'components/profile/profile.html',
        bindings: {},
        controller: profileCtrl
    });

    profileCtrl.$inject = ['$rootScope'];
    function profileCtrl($rootScope) {
        var $ctrl = this;
        $ctrl.loggedUser = $rootScope.loggedUser;
    }
})(angular.module('common.core'));