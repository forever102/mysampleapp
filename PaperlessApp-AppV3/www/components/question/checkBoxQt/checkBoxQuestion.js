/// <reference path="../../../../.vscode/typings/angularjs/angular.d.ts" />

(function(app) {
    app.component('checkBoxQuestion', {
        templateUrl: 'components/question/checkBoxQt/checkBoxQuestion.html',
        bindings: {
            question: '='
        },
        controller: checkBoxQuestionCtrl
    });

    checkBoxQuestionCtrl.$inject = ['$rootScope', '$scope', 'resultDetailRepo'];
    function checkBoxQuestionCtrl($rootScope, $scope, resultDetailRepo) {
        var $ctrl = this;

        /**
         * function areas.
         */
        $ctrl.saveAnswer = function() {
            if ($ctrl.question.answer) {                
                var jsonParams = { questionId: $ctrl.question.id, answeredContent: $ctrl.question.answer, resultId: $rootScope.currentResultId };
                resultDetailRepo.createOrReplace(jsonParams, function() {
                    console.info('saveDataEvent successfully.');
                });
            }            
        }

        /**
         * Listen to parent events.
         */

        /**
         * Starting point.
         */
        $ctrl.$onInit = function() {
        }
    }
})(angular.module('common.core'));