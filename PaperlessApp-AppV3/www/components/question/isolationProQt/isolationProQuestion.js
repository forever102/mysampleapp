/// <reference path="../../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.component('isolationProQuestion', {
        templateUrl: 'components/question/isolationProQt/isolationProQuestion.html',
        bindings: {
            question: '='
        },
        controller: isolationProQuestionCtrl
    });

    isolationProQuestionCtrl.$inject = ['$rootScope', '$scope', 'resultDetailRepo'];
    function isolationProQuestionCtrl($rootScope, $scope, resultDetailRepo) {
        var $ctrl = this;

        /**
         * Helper methods.
         */
        
        /**
         * function areas.
         */
        $ctrl.saveAnswer = function() {
            if ($ctrl.question.answer) {
                var jsonParams = { questionId: $ctrl.question.id, answeredContent: JSON.stringify($ctrl.question.answer), resultId: $rootScope.currentResultId };
                resultDetailRepo.createOrReplace(jsonParams, function() {
                    console.info('saveDataEvent successfully.');                    
                });
            }            
        }        

        /**
         * Starting point.
         */
        $ctrl.$onInit = function() {
            // Apply autosize for textarea.
            autosize(document.querySelectorAll('textarea'));                    

            // As this question has multiple answers, we need to customize its answer.
            if (!$ctrl.question.answer || $ctrl.question.answer == '') {
                $ctrl.question.answer = [
                    { 'placeholderContent': 'Step', 'answer': '' },
                    { 'placeholderContent': 'What needs isolating?', 'answer': '' },
                    { 'placeholderContent': 'Where is it isolated?', 'answer': '' },
                    { 'placeholderContent': 'How will it be isolated?', 'answer': '' },
                    { 'placeholderContent': 'Who will isolate it?', 'answer': '' },
                    { 'placeholderContent': 'How will isolation be proved', 'answer': '' },
                ];
            } else {
                // If its answer is already customized, then:
                if ($ctrl.question.answer && $ctrl.question.answer != '') {
                    $ctrl.question.answer = JSON.parse($ctrl.question.answer);
                }
            }
        }
    }
})(angular.module('common.core'));