/// <reference path="../../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.component('signatureQuestion', {        
        templateUrl: 'components/question/signatureQt/signatureQuestion.html',
        bindings: {
            question: '='
        },
        controller: signatureQuestionCtrl,
    });    

    signatureQuestionCtrl.$inject = ['$scope', 'drawingService', '$rootScope'];
    function signatureQuestionCtrl($scope, drawingService, $rootScope) {
        var $ctrl = this;

        var qId = $ctrl.question.id;

        /**
         * Helper methods.
         */
        function updateChanges() {
            setTimeout(function () {
                $scope.$apply(function () { });
            });
        }

        // $scope.signatureQuestionCtrlScope = $scope; // Pass scope other directives.
        // $scope.drawingPadDirectiveCtrlScope = {}; // Gain access to the scope of drawingPadDirectiveCtrl.
        $ctrl.signatureImageNameArr = [];
        $ctrl.urlForImageName = function (imageName) {
            if (!imageName || imageName == '') return null;
            else return cordova.file.dataDirectory + imageName;
        }
        $ctrl.showDrawingPadModal = function(signatureImageName, qId) {
            var loggedUser = $rootScope.loggedUser;
            var allowEdit = ($ctrl.signatureImageNameArr.length == 0) ? true: ((loggedUser && loggedUser.userName == '178848') ? true: false);
            // Trigger drawingPad modal component.
            // $scope.drawingPadDirectiveCtrlScope.initModal(signatureImageName, qId, allowEdit, function() {});
            $scope.$broadcast('showDrawingPadModalEvent', { 'editingImageName': signatureImageName, 'qId': qId, 'allowEdit': allowEdit });
        };
        
        /**
         * Starting point.
         */
        // $ctrl.init = function() {
        //     $ctrl.signatureImageNameArr[qId] = drawingService.getSignatureImageNameArrByQuestionId(qId);
        // };        
        // $ctrl.init();
        $ctrl.$onInit = function() {
            drawingService.getSignatureImageNameArrByQuestionId($ctrl.question.id, function (res) {
                $ctrl.signatureImageNameArr = res;
                updateChanges();
            });
        };
    }
})(angular.module('common.ui'));