(function (app) {
    app.component('questions', {
        templateUrl: 'components/question/questions.html',
        bindings: {},
        controller: questionsCtrl
    });

    questionsCtrl.$inject = ['$scope', '$stateParams', 'questionRepo', 'sectionRepo', '$ionicScrollDelegate', '$state', '$rootScope'];
    function questionsCtrl($scope, $stateParams, questionRepo, sectionRepo, $ionicScrollDelegate, $state, $rootScope) {
        var $ctrl = this;
        
        $ctrl.currentQuestionnaireId = $stateParams.questionnaireId;
        $ctrl.currentTerminalId = $stateParams.terminalId;
        $ctrl.currentSectionIndex = $stateParams.sectionIndex;
        $ctrl.currentSectionId = $stateParams.sectionId;        
        $ctrl.currentResultId = $stateParams.resultId;        
        $ctrl.questions = [];
        $ctrl.sections = [];
        $ctrl.stepQuestionArr = {};
        // $ctrl.uiMappings = [
        //     { 'questionTypeId': '1', 'questionTypeName': 'Text', 'ui': 'text' },
        //     { 'questionTypeId': '2', 'questionTypeName': 'Number', 'ui': 'text' },
        //     { 'questionTypeId': '3', 'questionTypeName': 'CheckBox', 'ui': 'checkbox' },
        //     { 'questionTypeId': '4', 'questionTypeName': 'Photo', 'ui': 'text' },
        //     { 'questionTypeId': '5', 'questionTypeName': 'Signature', 'ui': 'text' },
        //     { 'questionTypeId': '6', 'questionTypeName': 'Drawing', 'ui': 'text' },
        //     { 'questionTypeId': '7', 'questionTypeName': 'Bullet', 'ui': 'text' },
        //     { 'questionTypeId': '8', 'questionTypeName': 'DropDown', 'ui': 'text' },
        //     { 'questionTypeId': '9', 'questionTypeName': 'Label', 'ui': 'text' },
        //     { 'questionTypeId': '10', 'questionTypeName': 'Show Risk Assesment Image', 'ui': 'text' },
        //     { 'questionTypeId': '11', 'questionTypeName': 'Isolation Procedure', 'ui': 'text' },
        //     { 'questionTypeId': '12', 'questionTypeName': 'Risk Assesment Question', 'ui': 'text' },
        //     { 'questionTypeId': '13', 'questionTypeName': 'Show Custom Image', 'ui': 'text' }
        // ];

        /**
         * Helper methods.
         */
        function getQuestionsBySectionId(resultId, sectionId, successCb) {
            var questions = [];
            questionRepo.readByResultIdAndSectionId(resultId, sectionId).then(
                function (res) {
                    for (var i = 0; i < res.rows.length; i++) questions.push(res.rows.item(i));
                    successCb(questions);
                },
                function(err) {}
            );
        }

        function getSectionsByQuestionnaireIdAndTerminalId(questionnaireId, terminalId, successCb) {
            var sections = [];
            // sectionRepo.readByQuestionnaireIdAndTerminalId(questionnaireId, terminalId, function (tx, res) {
            //     for (var i = 0; i < res.rows.length; i++) sections.push(res.rows.item(i));
            //     successCb(sections);
            // });
            sectionRepo.readByResultId($ctrl.currentResultId).then(
                function(res) { 
                    for (var i = 0; i < res.rows.length; i++) sections.push(res.rows.item(i));
                    successCb(sections);
                 },
                function(err) {}
            );
        }
        // Reset some variables when move back or next.
        function resetSomeStuffs() {
            $ctrl.stepQuestionArr = {};                      
            // Update progress bar.
            $scope.$broadcast('updateProgressBarEvent', { 'resultId': $stateParams.resultId });
        }

        /**
         * Scopes area.
         */        
        // Handle Steps Questions (such as Risk Assesment, Isolation Procedure).        
        $ctrl.setIsAddedForStepQuestion = function(qId, answerArr) {
            var answerArr = JSON.parse(answerArr);
            var stepQuestion = { 'isAdded': false };
            $ctrl.stepQuestionArr[qId] = stepQuestion;
            $ctrl.disableAddMore = false;

            // Set isAdded to 'true' if answerArr has any answer.
            if (answerArr && answerArr.length > 0) {
                for (i = 0; i < answerArr.length; i++) {
                    if (answerArr[i].answer && answerArr[i].answer != '') {
                        $ctrl.stepQuestionArr[qId].isAdded = true;
                        break;
                    }
                }
            }            

            $ctrl.totalSteps = Object.keys($ctrl.stepQuestionArr).length;
        };        
        $ctrl.isFirstStepQuestion = function(qId) {
            var key = Object.keys($ctrl.stepQuestionArr)[0];
            if (key == qId) return true;
            return false;
        };
        $ctrl.addMoreStep = function() {
            var keys = Object.keys($ctrl.stepQuestionArr);
            for(i = 1; i < keys.length; i++){
                var qId = keys[i];
                var stepQuestion = $ctrl.stepQuestionArr[qId];
                if (stepQuestion.isAdded == false) {
                    stepQuestion.isAdded = true;                    
                    if (keys.length -1 == i) $ctrl.disableAddMore = true; // Disable 'Add More' button when it reaches the last one.
                    $ionicScrollDelegate.scrollBottom([shouldAnimate = true]); // This fixes the delay of showing the bottom's controls.
                    break;
                }
            }
            // console.log('$ctrl.stepQuestionArr is: ' + JSON.stringify($ctrl.stepQuestionArr));
        };
        // Handle moving Next and Back.
        $ctrl.moveNext = function () {            
            if ($ctrl.currentSectionIndex == $ctrl.sections.length - 1) return;
            
            resetSomeStuffs();            
            $ctrl.currentSectionIndex = parseInt($ctrl.currentSectionIndex) + 1;
            $ctrl.currentSectionId = $ctrl.sections[$ctrl.currentSectionIndex].id;            
            // console.log('$ctrl.currentSectionId is: ' + $ctrl.currentSectionId);

            // Update idSelectedSection of sections view.
            $state.go('.', {sectionId: $ctrl.currentSectionId}, {notify: false});

            getQuestionsBySectionId($ctrl.currentResultId, $ctrl.currentSectionId, function (questions) {
                $ctrl.questions = questions;
                setTimeout(function() {
                    $scope.$apply(function() {
                        $ionicScrollDelegate.resize(); // This fixed scroll delay.
                        $ionicScrollDelegate.scrollTop([shouldAnimate = true]);
                    });
                });                
            });            
        };
        $ctrl.moveBack = function () {
            resetSomeStuffs();
            $ionicScrollDelegate.scrollTop([shouldAnimate = true]);
            $ctrl.currentSectionIndex = parseInt($ctrl.currentSectionIndex) - 1;
            $ctrl.currentSectionId = $ctrl.sections[$ctrl.currentSectionIndex].id;
            // console.log('$ctrl.currentSectionId is: ' + $ctrl.currentSectionId);

            // Update idSelectedSection of sections view.
            $state.go('.', {sectionId: $ctrl.currentSectionId}, {notify: false});
            
            getQuestionsBySectionId($ctrl.currentResultId, $ctrl.currentSectionId, function (questions) {
                $ctrl.questions = questions;
                setTimeout(function() {
                    $scope.$apply(function() {
                        $ionicScrollDelegate.resize(); // This fixed scroll delay.
                        $ionicScrollDelegate.scrollTop([shouldAnimate = true]);
                    });
                });                
            });
        };

        /**
         * Listen to built-in and child events.
         */
        // $scope.$on('$ionicView.enter', function() {});

        $ctrl.$onInit = function() {
            // Load questions.
            if ($ctrl.currentSectionId && $ctrl.currentTerminalId && $ctrl.currentResultId) {
                getQuestionsBySectionId($ctrl.currentResultId, $ctrl.currentSectionId, function (questions) {
                    $ctrl.questions = questions;

                    // Update progress bar.
                    $scope.$broadcast('updateProgressBarEvent', { 'resultId': $stateParams.resultId });
                });
                getSectionsByQuestionnaireIdAndTerminalId($ctrl.currentQuestionnaireId, $ctrl.currentTerminalId, function (sections) {
                    $ctrl.sections = sections;
                });
                // Make sure resultId always exists for live reload mode.
                $rootScope.currentResultId = $ctrl.currentResultId;
            }
        };
    }
})(angular.module('common.core'));