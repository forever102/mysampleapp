/// <reference path="../../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.component('photoqt', {
        templateUrl: 'components/question/photoQt/photoqt.html',
        bindings: {
            question: '='
        },
        controller: photoqtCtrl
    });

    photoqtCtrl.$inject = ['$ionicModal', 'cameraService', '$scope'];
    function photoqtCtrl($ionicModal, cameraService, $scope) {
        var $ctrl = this;

        /**
         * Helper methods.
         */
        function updateChanges() {
            setTimeout(function () {
                $scope.$apply(function () { });
            });
        }

        /**
         * $ctrl area.
         */        
        $ctrl.imagesNames = [];
        // $ctrl.getImages = function (questionId) {
        //     var arr = [];
        //     cameraService.getImagesNamesByQuestionId(questionId, function(res) {
        //         arr = res;
        //     });
        //     console.info('arr is: ' + JSON.stringify(arr));
        //     return arr; 
        // };
        $ctrl.urlForImage = function(imageName) {
            if (!imageName || imageName == '') return null;

            var imagePath = cordova.file.dataDirectory + imageName;
            // console.log('imagePath is: ' + imagePath);

            return imagePath;
        };
        $ctrl.addMedia = function(questionId) {
            var options = {
                androidTheme: window.plugins.actionsheet.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT, // default is THEME_TRADITIONAL
                title: 'Add images',
                // subtitle: 'Choose wisely, my friend', // supported on iOS only
                buttonLabels: ['Take photo', 'Photo from library'],
                androidEnableCancelButton : true, // default false
                winphoneEnableCancelButton : true, // default false
                addCancelButtonWithLabel: 'Cancel',
                // addDestructiveButtonWithLabel : 'Delete it',
                // position: [20, 40], // for iPad pass in the [x, y] position of the popover
                // destructiveButtonLast: true // you can choose where the destructive button is shown
            };
            // Depending on the buttonIndex, you can now call shareViaFacebook or shareViaTwitter
            // of the SocialSharing plugin (https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin)
            window.plugins.actionsheet.show(options, function(buttonIndex) {
                if (buttonIndex <= 2) {
                    // the buttonIndex is 1-based (first button is index 1).                
                    cameraService.saveImage(buttonIndex - 1, questionId, function() {
                        updateChanges();                        
                    });
                }
            });
        };
        $ctrl.editImage = function(imageName, questionId) {
            // $ctrl.editImageModal.show();
            // $ctrl.editingImageUrl = $ctrl.urlForImage(imageName);            
            // $ctrl.editingQuestionId = questionId;            
            // $ctrl.showBar = true;

            // Trigger image editor component.
            $scope.$broadcast('showImageEditorEvent', { 'imageName': imageName, 'questionId': questionId });
            // $scope.$broadcast('showDarkroomEditorEvent', { 'imageName': imageName, 'questionId': questionId });
        };

        /**
         * Starting point.
         */
        $ctrl.$onInit = function() {   
            cameraService.getImagesNamesByQuestionId($ctrl.question.id, function(res) {
                $ctrl.imagesNames = res;
                updateChanges();
            });
        }        
    }
})(angular.module('common.core'));