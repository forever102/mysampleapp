/// <reference path="../../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.component('riskassqt', {
        templateUrl: 'components/question/riskAssQt/riskassqt.html',
        bindings: {
            question: '='
        },
        controller: riskassqtCtrl
    });

    riskassqtCtrl.$inject = ['$rootScope', '$scope', 'resultDetailRepo'];
    function riskassqtCtrl($rootScope, $scope, resultDetailRepo) {
        var $ctrl = this;        
        
        /**
         * Helper methods.
         */
        
        /**
         * function areas.
         */
        $ctrl.saveAnswer = function() {
            if ($ctrl.question.answer && $rootScope.currentResultId) {
                var jsonParams = { questionId: $ctrl.question.id, answeredContent: JSON.stringify($ctrl.question.answer), resultId: $rootScope.currentResultId };
                resultDetailRepo.createOrReplace(jsonParams, function() {
                    console.info('saveDataEvent successfully.');
                });
            }            
        }

        /**
         * Starting point.
         */
        $ctrl.$onInit = function() {
            // Apply autosize for textarea.
            autosize(document.querySelectorAll('textarea'));

            // As this question has multiple answers, we need to customize its answer.
            if (!$ctrl.question.answer || $ctrl.question.answer == '') {
                $ctrl.question.answer = [
                    { 'placeholderContent': `Job step/ Sequence:
                    Break work sequence into clear separate job steps to follow i.e. Step 1 - Isolate/ de-energise machinery & remove cover.`, 'answer': '' },
                    { 'placeholderContent': 'Hazards', 'answer': '' },
                    { 'placeholderContent': 'Risk', 'answer': '' },
                    { 'placeholderContent': 'Raw Risk Score (without controls)', 'answer': '' },
                    { 'placeholderContent': 'Risk Controls', 'answer': '' },
                    { 'placeholderContent': 'Residual Risk Score (without controls)', 'answer': '' },
                    { 'placeholderContent': 'Action', 'answer': '' }
                ];
            } else {
                // If its answer is already customized, then:
                if ($ctrl.question.answer && $ctrl.question.answer != '') {
                    $ctrl.question.answer = JSON.parse($ctrl.question.answer);
                }
            }                    
        }
    }
})(angular.module('common.core'));