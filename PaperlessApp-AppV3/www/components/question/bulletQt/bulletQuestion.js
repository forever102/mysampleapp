
/// <reference path="../../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.component('bulletQuestion', {
        templateUrl: 'components/question/bulletQt/bulletQuestion.html',
        bindings: {
            question: '='
        },
        controller: bulletQuestionCtrl
    });

    bulletQuestionCtrl.$inject = ['$rootScope', 'resultDetailRepo'];
    function bulletQuestionCtrl($rootScope, resultDetailRepo) {
        var $ctrl = this;
        
        /**
         * function areas.
         */
        $ctrl.saveAnswer = function() {
            if ($ctrl.question.answer) {                
                var jsonParams = { questionId: $ctrl.question.id, answeredContent: $ctrl.question.answer, resultId: $rootScope.currentResultId };                
                resultDetailRepo.createOrReplace(jsonParams, function() {
                    console.info('saveDataEvent successfully.');
                });
            }            
        }

        /**
         * Starting point.
         */
        $ctrl.$onInit = function() { 
            var contentArr = $ctrl.question.content.split('?');
            $ctrl.contentTitle = contentArr[0];
            $ctrl.contentOptionsArr = contentArr[1].split('|');
        }
    }
})(angular.module('common.core'));