/// <reference path="../../../../.vscode/typings/angularjs/angular.d.ts" />

(function(app) {
    app.component('textQuestion', {
        templateUrl: 'components/question/textQt/textQuestion.html',
        bindings: {
            question: '='
        },
        controller: textQuestionCtrl
    });

    textQuestionCtrl.$inject = ['$rootScope', 'resultDetailRepo'];
    function textQuestionCtrl($rootScope, resultDetailRepo) {
        var $ctrl = this;

        /**
         * function areas.
         */
        $ctrl.saveAnswer = function() {
            if ($ctrl.question.answer) {                
                var jsonParams = { questionId: $ctrl.question.id, answeredContent: $ctrl.question.answer, resultId: $rootScope.currentResultId };
                resultDetailRepo.createOrReplace(jsonParams, function() {
                    console.info('saveDataEvent successfully.');
                });
            }            
        }

        /**
         * Listen to parent events.
         */

        /**
         * Starting point.
         */
        $ctrl.$onInit = function() {            
        }
    }
})(angular.module('common.core'));