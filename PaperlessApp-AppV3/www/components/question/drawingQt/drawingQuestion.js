/// <reference path="../../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.component('drawingQuestion', {        
        templateUrl: 'components/question/drawingQt/drawingQuestion.html',
        bindings: {
            question: '='
        },
        controller: drawingQuestionCtrl,
    });    

    drawingQuestionCtrl.$inject = ['$scope', 'drawingService'];
    function drawingQuestionCtrl($scope, drawingService) {
        var $ctrl = this;

        var qId = $ctrl.question.id;        

        /**
         * Helper methods.
         */
        function updateChanges() {
            setTimeout(function () {
                $scope.$apply(function () { });
            });
        }

        /**
         * $ctrl area.
         */
        $ctrl.drawingImageNameArr = [];
        $ctrl.urlForImageName = function (imageName) {
            if (!imageName || imageName == '') return null;
            else return cordova.file.dataDirectory + imageName;
        }
        $ctrl.showDrawingPadModal = function(drawingImageName, qId) {
            var allowEdit = true;
            // Trigger drawingPad modal component.
            $scope.$broadcast('showDrawingPadModalEvent', { 'editingImageName': drawingImageName, 'qId': qId, 'allowEdit': allowEdit });
        };

        /**
         * Listen to events emitted from child.
         */        
        
        // $ctrl.init = function() {
        //     $ctrl.drawingImageNameArr[qId] = drawingService.getSignatureImageNameArrByQuestionId(qId);
        // };        
        // $ctrl.init();
        $ctrl.$onInit = function() {
            drawingService.getSignatureImageNameArrByQuestionId($ctrl.question.id, function(res) {
                $ctrl.drawingImageNameArr = res;
                updateChanges();
            });
        }
    }
})(angular.module('common.ui'));