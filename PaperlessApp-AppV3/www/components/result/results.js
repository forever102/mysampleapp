/// <reference path="../../../.vscode/typings/angularjs/angular.d.ts" />

(function (app) {
    app.component('results', {
        templateUrl: 'components/result/results.html',
        bindings: {
            text: '='
        },
        controller: resultsCtrl
    });

    resultsCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'resultRepo'];
    function resultsCtrl($rootScope, $scope, $state, $stateParams, resultRepo) {
        var $ctrl = this;
        /**
         * Helper methods.
         */
        function loadData() {
            $ctrl.results = [];
            var jsonParams = { 'userName': $rootScope.loggedUser.userName, 'questionnaireId': $stateParams.questionnaireId, 'terminalId': $stateParams.terminalId };
            return resultRepo.readByQuestionnaireAndTerminalId(jsonParams).then(
                function (res) {
                    for (var i = 0; i < res.rows.length; i++) $ctrl.results.push(res.rows.item(i));
                    // console.info('$ctrl.results is: ' + JSON.stringify($ctrl.results));
                },
                function (err) { }
            );
        }

        /**
         * $ctrl area.
         */        
        $ctrl.selectResult = function (idSelectedResult) {
            $ctrl.idSelectedResult = idSelectedResult;
            // Set current resultId as global variable for all sections, questions and services to access.
            $rootScope.currentResultId = idSelectedResult;
            $state.go('home.sections', { questionnaireId: $stateParams.questionnaireId, terminalId: $stateParams.terminalId, resultId: idSelectedResult });
        };
        $ctrl.createNewResult = function () {
            // Create new result.
            var jsonParams = { 'userName': $rootScope.loggedUser.userName, 'questionnaireId': $stateParams.questionnaireId, 'terminalId': $stateParams.terminalId };
            resultRepo.create(jsonParams, function (newInsertId) {
                if (newInsertId) {
                    $rootScope.idSelectedResult = newInsertId;
                    $state.go('home.sections', { questionnaireId: $stateParams.questionnaireId, terminalId: $stateParams.terminalId, resultId: newInsertId });

                    // var newResult = res.rows.item(0);
                    // // console.info('newResult is: ' + JSON.stringify(newResult));
                    // if (newResult) {
                    //     $ctrl.results.push(newResult);
                    //     $ctrl.idSelectedResult = newResult.id;
                    //     $state.go('home.sections', { questionnaireId: $stateParams.questionnaireId, terminalId: $stateParams.terminalId, resultId: newResult.id }); 
                    // }                    
                }
            });
        }

        /**
         * Listen to built-in and custom events.
         */        
        // Refresh results if getting back from sections view.
        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            if (fromState.name == 'home.sections' && toState.name == 'home.results') {                
                loadData();
            }            
        });

        /**
         * Starting point.
         */
        $ctrl.$onInit = function() {
            if ($stateParams.questionnaireId && $stateParams.terminalId) {
                loadData();
            }  
        };
    }
})(angular.module('common.core'));