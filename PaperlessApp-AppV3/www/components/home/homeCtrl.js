/// <reference path="../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.controller('homeCtrl', homeCtrl);
    
    homeCtrl.$inject = ['$state', 'appDbContext', 'appDbContextSeedData'];
    function homeCtrl($state, appDbContext, appDbContextSeedData) {
        var $ctrl = this;

        /**
         * Variables areas.
         */

        /**
         * Helper methods.
         */
        function syncData(successCb) {
            // NOTE Here: if the app gets error while syncing data from server, then a message dialog should be shown. It still continues to work with the offline sample data.
            getDataFromServer(function (data) {
                console.info('data is: ' + JSON.stringify(data));
                appDbContext.syncDataWithServer(data, function() { 
                    successCb(); 
                });
            });
        };        

        // Helper methods.
        function getDataFromServer(successCb) {
            // Read xml data from damstra API.
            var apiUrl = 'https://api.damstra.com.au/InstallService.svc/GetHardWareTypes';
            var jsonData = null;
            // // $http.defaults.headers.common['auth-token'] = 'KTRQJBUDBEQJGXEZDPFYLYJAOPZCOSJY';
            // $http.get('https://api.damstra.com.au/InstallService.svc/GetHardWareTypes')
            // .then(
            //     function (response) {
            //         // var x2js = new X2JS();
            //         // var jsonData = x2js.xml_str2json(response.data);
            //         console.log('response is: ' + response);
            //         // console.log('jsonData is: ' + JSON.stringify(jsonData));
            //     }, 
            //     function (error) {
            //         // Should use modal dialog here.
            //         console.log('Cannot read data from api services. Error is: ' + JSON.stringify(error));
            //     })
            // .finally(function () { });
            successCb(jsonData);
        }

        function seedDb(successCb, failureCb) {
            // Drop DB if you want to refresh DB. NOTE: run either dropDb or seedDataFromServer as it's asynchronous.
            // appDbContext.dropDb();
            
            // Seed sample data if the initial db is empty. If there's any changes in sample data, then drop DB and re-run the app again.
            appDbContextSeedData.seedSampleData(
                function (res) {
                    // The app will sync data with the server everytime it gets started or users use "Pull to Refresh" feature.
                    syncData(function() { 
                        // successCb();
                    });
                    successCb();
                },
                function (error) { 
                    console.log('appDbContextSeedData.seedSampleData failed. Error is: ' + JSON.stringify(error)); 
                    failureCb();
                }
            );
        };
        
        /**
         * $ctrl areas.
         */        
        $ctrl.logout = function() {
            window.localStorage.removeItem('loggedUser');
            $state.go('tab.login');
        }
        $ctrl.clickNavBackBtn = function() {
            // console.info('$state.current.name is: ' + $state.current.name);
        }

        /**
         * Starting point.
         */
         $ctrl.init = function() { };
         $ctrl.init();
    }
})(angular.module('common.core'));