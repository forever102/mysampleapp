/// <reference path="../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.component('tabs', {
        templateUrl: 'components/tabs/tabs.html',
        bindings: {
            text: '='
        },
        controller: tabsCtrl
    });

    tabsCtrl.$inject = [];
    function tabsCtrl() {
        var $ctrl = this;
        $ctrl.title = 'tabs component';
    }    
})(angular.module('common.core'));