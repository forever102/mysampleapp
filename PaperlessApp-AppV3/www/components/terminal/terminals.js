(function (app) {
    app.component('terminals', {
        templateUrl: 'components/terminal/terminals.html',
        bindings: {},
        controller: terminalsCtrl
    });

    terminalsCtrl.$inject = ['$state', '$stateParams', 'terminalRepo'];
    function terminalsCtrl($state, $stateParams, terminalRepo) {
        var $ctrl = this;
                
        $ctrl.selectTerminal = function (questionnaireId, idSelectedTerminal) {
            $ctrl.idSelectedTerminal = idSelectedTerminal;
            $state.go('home.results', { questionnaireId: questionnaireId, terminalId: idSelectedTerminal });
        };

        $ctrl.$onInit = function() {
            var questionnaireId = $stateParams.questionnaireId;
            if (questionnaireId) {
                $ctrl.questionnaireId = questionnaireId;
                $ctrl.terminals = [];
                return terminalRepo.readByQuestionnaireId(questionnaireId).then(
                    function (res) {
                        for (var i = 0; i < res.rows.length; i++) $ctrl.terminals.push(res.rows.item(i));
                    },
                    function(err) {}
                );
            }
        }
    }
})(angular.module('common.core'));