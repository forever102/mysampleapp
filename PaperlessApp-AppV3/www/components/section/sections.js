(function (app) {
    app.component('sections', {
        templateUrl: 'components/section/sections.html',
        bindings: {},
        controller: sectionsCtrl
    });

    sectionsCtrl.$inject = ['$scope', '$stateParams', 'sectionRepo', '$state', '$rootScope', 'resultRepo'];
    function sectionsCtrl($scope, $stateParams, sectionRepo, $state, $rootScope, resultRepo) {
        var $ctrl = this;

        /**
         * Helper methods.
         */
        function loadData() {            
            $ctrl.sections = [];
            sectionRepo.readByResultId($stateParams.resultId).then(
                function(res) {
                    for (var i = 0; i < res.rows.length; i++) $ctrl.sections.push(res.rows.item(i));
                    // Update progress bar.
                    $scope.$broadcast('updateProgressBarEvent', { 'resultId': $stateParams.resultId });
                },
                function(err) {}
            );
        }
        
        /**
         * $ctrl area.
         */        
        $ctrl.selectSection = function (idSelectedSection, sectionIndex) {
            $ctrl.idSelectedSection = idSelectedSection;
            $state.go('home.questions', { questionnaireId: $stateParams.questionnaireId, terminalId: $stateParams.terminalId, sectionId: idSelectedSection, sectionIndex: sectionIndex, resultId: $stateParams.resultId });
        };

        /**
         * Listen to built-in and custom events.
         */
        // Refresh sections if getting back from questions view.
        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            // console.info('toParams is: ' + JSON.stringify(toParams) + ', fromParams is: ' + JSON.stringify(fromParams));
            if (fromState.name == 'home.questions' && toState.name == 'home.sections') {                
                loadData();
                $ctrl.idSelectedSection = fromParams.sectionId;
            }            
        });

        $ctrl.$onInit = function() {
            // Load sections.
            if ($stateParams.resultId) {   
                $ctrl.sections = [];
                loadData();
                // Update the global resultId value.
                $rootScope.currentResultId = $stateParams.resultId;
            }
        }
    }
})(angular.module('common.core'));