(function (app) {
    app.component('questionnaires', {
        templateUrl: 'components/questionnaire/questionnaires.html',
        bindings: {},
        controller: questionnairesCtrl
    });

    questionnairesCtrl.$inject = ['$state', 'questionnaireRepo'];
    function questionnairesCtrl($state, questionnaireRepo) {
        var $ctrl = this;

        /**
         * Helper methods.
         */
        
        $ctrl.selectQuestionnaire = function (idSelectedQuestionnaire) {
            $ctrl.idSelectedQuestionnaire = idSelectedQuestionnaire;
            $state.go('home.terminals', { questionnaireId: idSelectedQuestionnaire });
        };

        $ctrl.$onInit = function() {
            $ctrl.questionnaires = [];
            // Must be from DB based on user's skillId.
            // var questionnaires = [
            //     {id: 1, name: 'DamstraHolcim LV Electrical Work Risk Assessment'},
            //     {id: 2, name: 'Job Safety & Environmental Analysis'}
            // ];
            return questionnaireRepo.readAll().then(
                function (res) {
                    for (var i = 0; i < res.rows.length; i++) $ctrl.questionnaires.push(res.rows.item(i));
                },
                function (err) {}
            );
        }
    }
})(angular.module('common.core'));