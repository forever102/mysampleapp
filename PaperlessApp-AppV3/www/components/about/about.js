/// <reference path="../../../.vscode/typings/angularjs/angular.d.ts"/>

(function(app) {
    app.component('about', {
        templateUrl: 'components/about/about.html',
        bindings: {
            text: '='
        },
        controller: aboutCtrl
    });

    aboutCtrl.$inject = [];
    function aboutCtrl() {
        var $ctrl = this;
        $ctrl.title = 'sample component template';
    }
})(angular.module('common.core'));