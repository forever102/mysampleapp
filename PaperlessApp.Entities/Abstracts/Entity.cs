﻿using System;

namespace PaperlessApp.Entities.Abstracts
{
    public abstract class Entity
    {
        public Entity()
        { }

        public virtual int Id { get; set; }
        // public virtual Guid GloUniId { get; set; }
        // public virtual DateTimeOffset CreatedDate { get; set; }
        // public virtual DateTimeOffset UpdatedDate { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
