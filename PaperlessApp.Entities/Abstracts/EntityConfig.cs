﻿using System;
using Microsoft.EntityFrameworkCore;

namespace PaperlessApp.Entities.Abstracts
{
    public class EntityConfig<T> where T : Entity
    {
        public EntityConfig(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<T>().HasKey(t => t.Id);

            // modelBuilder.Entity<T>()
            //     .Property(t => t.GloUniId)
            //     .HasDefaultValueSql(PostgresHelper.GetGuid());
            // modelBuilder.Entity<T>().HasIndex(t => t.GloUniId).IsUnique();

            modelBuilder.Entity<T>().Property(t => t.CreatedDate)
                .HasColumnType("DateTime")
                .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

            modelBuilder.Entity<T>().Property(t => t.UpdatedDate)
                .HasColumnType("DateTime")
                .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");
        }
    }
}
