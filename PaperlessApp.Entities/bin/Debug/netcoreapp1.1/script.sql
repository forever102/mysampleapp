﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" TEXT NOT NULL CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY,
    "ProductVersion" TEXT NOT NULL
);

CREATE TABLE "AppRoles" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_AppRoles" PRIMARY KEY AUTOINCREMENT,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "Name" TEXT,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW'))
);

CREATE TABLE "AppUsers" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_AppUsers" PRIMARY KEY AUTOINCREMENT,
    "AccessFailedCount" INTEGER NOT NULL DEFAULT 0,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "Email" TEXT,
    "EmailConfirmed" INTEGER NOT NULL DEFAULT 0,
    "Gender" TEXT,
    "IsLocked" INTEGER NOT NULL DEFAULT 0,
    "LockoutEnabled" INTEGER NOT NULL DEFAULT 0,
    "LockoutEnd" TEXT NOT NULL DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')),
    "PasswordHash" TEXT,
    "PhoneNumber" TEXT,
    "PhoneNumberConfirmed" INTEGER NOT NULL DEFAULT 0,
    "Salt" TEXT,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "UserName" TEXT
);

CREATE TABLE "QuestionTypes" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_QuestionTypes" PRIMARY KEY AUTOINCREMENT,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "QuestionTypeName" TEXT,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW'))
);

CREATE TABLE "Terminals" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_Terminals" PRIMARY KEY AUTOINCREMENT,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "Name" TEXT,
    "Order" INTEGER,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW'))
);

CREATE TABLE "UIControls" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_UIControls" PRIMARY KEY AUTOINCREMENT,
    "CodeName" TEXT,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "Name" TEXT,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW'))
);

CREATE TABLE "AppUserRoles" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_AppUserRoles" PRIMARY KEY AUTOINCREMENT,
    "AppRoleId" INTEGER NOT NULL,
    "AppUserId" INTEGER NOT NULL,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    CONSTRAINT "AK_AppUserRoles_AppUserId_AppRoleId" UNIQUE ("AppUserId", "AppRoleId"),
    CONSTRAINT "FK_AppUserRoles_AppRoles_AppRoleId" FOREIGN KEY ("AppRoleId") REFERENCES "AppRoles" ("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_AppUserRoles_AppUsers_AppUserId" FOREIGN KEY ("AppUserId") REFERENCES "AppUsers" ("Id") ON DELETE CASCADE
);

CREATE TABLE "Errors" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_Errors" PRIMARY KEY AUTOINCREMENT,
    "AppUserId" INTEGER,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "Message" TEXT,
    "StackTrace" TEXT,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    CONSTRAINT "FK_Errors_AppUsers_AppUserId" FOREIGN KEY ("AppUserId") REFERENCES "AppUsers" ("Id") ON DELETE RESTRICT
);

CREATE TABLE "Skills" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_Skills" PRIMARY KEY AUTOINCREMENT,
    "AppUserId" INTEGER,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "Name" TEXT,
    "Order" INTEGER,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    CONSTRAINT "FK_Skills_AppUsers_AppUserId" FOREIGN KEY ("AppUserId") REFERENCES "AppUsers" ("Id") ON DELETE RESTRICT
);

CREATE TABLE "Questionnaires" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_Questionnaires" PRIMARY KEY AUTOINCREMENT,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "Name" TEXT,
    "Order" INTEGER,
    "SkillId" INTEGER,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    CONSTRAINT "FK_Questionnaires_Skills_SkillId" FOREIGN KEY ("SkillId") REFERENCES "Skills" ("Id") ON DELETE RESTRICT
);

CREATE TABLE "UserSkills" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_UserSkills" PRIMARY KEY AUTOINCREMENT,
    "AppUserId" INTEGER NOT NULL,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "SkillId" INTEGER NOT NULL,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    CONSTRAINT "AK_UserSkills_AppUserId_SkillId" UNIQUE ("AppUserId", "SkillId"),
    CONSTRAINT "FK_UserSkills_AppUsers_AppUserId" FOREIGN KEY ("AppUserId") REFERENCES "AppUsers" ("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_UserSkills_Skills_SkillId" FOREIGN KEY ("SkillId") REFERENCES "Skills" ("Id") ON DELETE CASCADE
);

CREATE TABLE "QuestionnaireTerminals" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_QuestionnaireTerminals" PRIMARY KEY AUTOINCREMENT,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "QuestionnaireId" INTEGER NOT NULL,
    "TerminalId" INTEGER NOT NULL,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    CONSTRAINT "AK_QuestionnaireTerminals_QuestionnaireId_TerminalId" UNIQUE ("QuestionnaireId", "TerminalId"),
    CONSTRAINT "FK_QuestionnaireTerminals_Questionnaires_QuestionnaireId" FOREIGN KEY ("QuestionnaireId") REFERENCES "Questionnaires" ("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_QuestionnaireTerminals_Terminals_TerminalId" FOREIGN KEY ("TerminalId") REFERENCES "Terminals" ("Id") ON DELETE CASCADE
);

CREATE TABLE "Results" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_Results" PRIMARY KEY AUTOINCREMENT,
    "AppUserId" INTEGER NOT NULL,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "QuestionnaireTerminalId" INTEGER NOT NULL,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "isComplete" INTEGER NOT NULL DEFAULT 0,
    CONSTRAINT "FK_Results_AppUsers_AppUserId" FOREIGN KEY ("AppUserId") REFERENCES "AppUsers" ("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_Results_QuestionnaireTerminals_QuestionnaireTerminalId" FOREIGN KEY ("QuestionnaireTerminalId") REFERENCES "QuestionnaireTerminals" ("Id") ON DELETE CASCADE
);

CREATE TABLE "Sections" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_Sections" PRIMARY KEY AUTOINCREMENT,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "Name" TEXT,
    "Order" INTEGER,
    "QuestionnaireTerminalId" INTEGER NOT NULL,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    CONSTRAINT "FK_Sections_QuestionnaireTerminals_QuestionnaireTerminalId" FOREIGN KEY ("QuestionnaireTerminalId") REFERENCES "QuestionnaireTerminals" ("Id") ON DELETE CASCADE
);

CREATE TABLE "Questions" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_Questions" PRIMARY KEY AUTOINCREMENT,
    "Content" TEXT,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "Order" INTEGER,
    "QuestionTypeId" INTEGER NOT NULL,
    "SectionId" INTEGER NOT NULL,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    CONSTRAINT "FK_Questions_QuestionTypes_QuestionTypeId" FOREIGN KEY ("QuestionTypeId") REFERENCES "QuestionTypes" ("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_Questions_Sections_SectionId" FOREIGN KEY ("SectionId") REFERENCES "Sections" ("Id") ON DELETE CASCADE
);

CREATE TABLE "ResultDetails" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_ResultDetails" PRIMARY KEY AUTOINCREMENT,
    "AnsweredContent" TEXT,
    "CreatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    "QuestionId" INTEGER NOT NULL,
    "ResultId" INTEGER NOT NULL,
    "UpdatedDate" DateTime NOT NULL DEFAULT (STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')),
    CONSTRAINT "AK_ResultDetails_ResultId_QuestionId" UNIQUE ("ResultId", "QuestionId"),
    CONSTRAINT "FK_ResultDetails_Questions_QuestionId" FOREIGN KEY ("QuestionId") REFERENCES "Questions" ("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_ResultDetails_Results_ResultId" FOREIGN KEY ("ResultId") REFERENCES "Results" ("Id") ON DELETE CASCADE
);

CREATE UNIQUE INDEX "IX_AppRoles_Name" ON "AppRoles" ("Name");

CREATE UNIQUE INDEX "IX_AppUsers_Email" ON "AppUsers" ("Email");

CREATE UNIQUE INDEX "IX_AppUsers_UserName" ON "AppUsers" ("UserName");

CREATE INDEX "IX_AppUserRoles_AppRoleId" ON "AppUserRoles" ("AppRoleId");

CREATE INDEX "IX_Errors_AppUserId" ON "Errors" ("AppUserId");

CREATE INDEX "IX_Questions_QuestionTypeId" ON "Questions" ("QuestionTypeId");

CREATE INDEX "IX_Questions_SectionId" ON "Questions" ("SectionId");

CREATE UNIQUE INDEX "IX_Questionnaires_Name" ON "Questionnaires" ("Name");

CREATE INDEX "IX_Questionnaires_SkillId" ON "Questionnaires" ("SkillId");

CREATE INDEX "IX_QuestionnaireTerminals_TerminalId" ON "QuestionnaireTerminals" ("TerminalId");

CREATE INDEX "IX_Results_AppUserId" ON "Results" ("AppUserId");

CREATE INDEX "IX_Results_QuestionnaireTerminalId" ON "Results" ("QuestionnaireTerminalId");

CREATE INDEX "IX_ResultDetails_QuestionId" ON "ResultDetails" ("QuestionId");

CREATE INDEX "IX_Sections_QuestionnaireTerminalId" ON "Sections" ("QuestionnaireTerminalId");

CREATE INDEX "IX_Skills_AppUserId" ON "Skills" ("AppUserId");

CREATE UNIQUE INDEX "IX_Terminals_Name" ON "Terminals" ("Name");

CREATE INDEX "IX_UserSkills_SkillId" ON "UserSkills" ("SkillId");

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20170329070957_UpdateGeneratedDatetime', '1.1.0-rtm-22752');

