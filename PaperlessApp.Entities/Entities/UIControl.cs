﻿using PaperlessApp.Entities.Abstracts;

namespace PaperlessApp.Entities.Entities
{
    public class UIControl : Entity
    {
        public string Name { get; set; }
        public string CodeName { get; set; }
    }
}
