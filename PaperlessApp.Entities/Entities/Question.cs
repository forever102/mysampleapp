﻿using PaperlessApp.Entities.Abstracts;

namespace PaperlessApp.Entities.Entities
{
    public class Question : Entity
    {        
        public string Content { get; set; }
        public int? Order { get; set; }

        public int SectionId { get; set; }
        public Section Section { get; set; }

        public int QuestionTypeId { get; set; }
        public QuestionType QuestionType { get; set; }
    }
}
