﻿using PaperlessApp.Entities.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperlessApp.Entities.Entities
{
    public class Error : Entity
    {
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public int? AppUserId { get; set; } // Allow this to be Null as Excpetion can be logged before logging in.
        public AppUser AppUser { get; set; }
    }
}
