﻿using PaperlessApp.Entities.Abstracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PaperlessApp.Entities.Entities
{
    public class AppUser : Entity
    {
        public AppUser()
        { }

        public int AccessFailedCount { get; set; }

        [MaxLength(256)]
        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }
        public string Gender { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTimeOffset LockoutEnd { get; set; }

        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }

        [MaxLength(256)]
        public string UserName { get; set; }

        // Additional fields.
        public string Salt { get; set; }
        public bool IsLocked { get; set; }        
        //public ICollection<ToDo> ToDos { get; set; }
        public ICollection<Error> Errors { get; set; }
        public ICollection<AppUserRole> AppUserRoles { get; set; }
        public ICollection<Skill> Skills { get; set; }
        public ICollection<Result> Results { get; set; }
    }
}
