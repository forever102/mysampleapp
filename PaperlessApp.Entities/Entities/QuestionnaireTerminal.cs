using PaperlessApp.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace PaperlessApp.Entities.Entities
{
    public class QuestionnaireTerminal : Entity
    {
        [Required]
        public int QuestionnaireId { get; set; }
        [Required]
        public int TerminalId { get; set; }

        public Questionnaire Questionnaire { get; set; }
        public Terminal Terminal { get; set; }

        public ICollection<Section> Sections { get; set; }
        public ICollection<Result> Results { get; set; }
    }
}