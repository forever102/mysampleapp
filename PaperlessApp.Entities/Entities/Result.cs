﻿using PaperlessApp.Entities.Abstracts;
using System.Collections.Generic;

namespace PaperlessApp.Entities.Entities
{
    public class Result : Entity
    {
        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }

        public int QuestionnaireTerminalId { get; set; }
        public QuestionnaireTerminal QuestionnaireTerminal { get; set; }

        public bool isComplete { get; set; }

        public ICollection<ResultDetail> ResultDetails { get; set; }
    }
}
