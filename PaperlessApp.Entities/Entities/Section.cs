﻿using PaperlessApp.Entities.Abstracts;
using System.Collections.Generic;

namespace PaperlessApp.Entities.Entities
{
    public class Section : Entity
    {
        public string Name { get; set; }
        public int? Order { get; set; }

        // public int TerminalId { get; set; }
        // public Terminal Terminal { get; set; }
        public int QuestionnaireTerminalId { get; set; }
        public QuestionnaireTerminal QuestionnaireTerminal { get; set; }
        public ICollection<Question> Questions { get; set; }
    }
}
