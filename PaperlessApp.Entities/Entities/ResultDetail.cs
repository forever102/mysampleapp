﻿using PaperlessApp.Entities.Abstracts;

namespace PaperlessApp.Entities.Entities
{
    public class ResultDetail : Entity
    {
        public int QuestionId { get; set; }
        public Question Question { get; set; }
        public string AnsweredContent { get; set; }

        public int ResultId { get; set; }
        public Result Result { get; set; }
    }
}
