﻿using PaperlessApp.Entities.Abstracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PaperlessApp.Entities.Entities
{
    public class AppRole : Entity
    {
        public AppRole()
        { }

        // Added ones.
        [MaxLength(256)]
        public string Name { get; set; }

        public ICollection<AppUserRole> AppUserRoles { get; set; }
    }
}
