﻿using PaperlessApp.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;

namespace PaperlessApp.Entities.Entities
{
    public class AppUserRole : Entity
    {
        [Required]
        public int AppUserId { get; set; }
        [Required]
        public int AppRoleId { get; set; }

        public AppUser AppUser { get; set; }
        public AppRole AppRole { get; set; }
    }
}
