﻿using PaperlessApp.Entities.Abstracts;
using System.Collections.Generic;

namespace PaperlessApp.Entities.Entities
{
    public class Questionnaire : Entity
    {        
        public string Name { get; set; }
        public int? Order { get; set; }

        public int? SkillId { get; set; }
        public Skill Skill { get; set; }
        // public ICollection<Section> Sections { get; set; }
        public ICollection<QuestionnaireTerminal> QuestionnaireTerminals { get; set; }
    }
}
