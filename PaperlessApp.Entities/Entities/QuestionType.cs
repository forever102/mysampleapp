using System.Collections.Generic;
using PaperlessApp.Entities.Abstracts;

namespace PaperlessApp.Entities.Entities
{
    public class QuestionType : Entity
    {        
        public string QuestionTypeName { get; set; }

        public ICollection<Question> Questions { get; set; }
    }
}
