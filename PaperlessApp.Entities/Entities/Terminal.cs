using PaperlessApp.Entities.Abstracts;
using System.Collections.Generic;

namespace PaperlessApp.Entities.Entities
{
    public class Terminal : Entity
    {         
        public string Name { get; set; }
        public int? Order { get; set; }
        
        // public int QuestionnaireId { get; set; }
        // public Questionnaire Questionnaire { get; set; }

        // public ICollection<Section> Sections { get; set; }
        public ICollection<QuestionnaireTerminal> QuestionnaireTerminals { get; set; }
    }
}