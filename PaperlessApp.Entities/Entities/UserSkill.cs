﻿using PaperlessApp.Entities.Abstracts;
using System.ComponentModel.DataAnnotations;

namespace PaperlessApp.Entities.Entities
{
    // Linking table between AppUsers and Skills.
    public class UserSkill : Entity
    {
        [Required]
        public int AppUserId { get; set; }

        [Required]
        public int SkillId { get; set; }

        public AppUser AppUser { get; set; }
        public Skill Skill { get; set; }
    }
}
