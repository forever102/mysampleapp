﻿using PaperlessApp.Entities.Abstracts;
using System.Collections.Generic;

namespace PaperlessApp.Entities.Entities
{
    public class Skill : Entity
    {
        public string Name { get; set; }
        public int? Order { get; set; }

        public ICollection<UserSkill> UserSkills { get; set; }
        public ICollection<Questionnaire> Questionnaires { get; set; }
    }
}
