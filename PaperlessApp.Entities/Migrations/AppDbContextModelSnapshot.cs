﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using PaperlessApp.Entities;

namespace PaperlessApp.Entities.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752");

            modelBuilder.Entity("PaperlessApp.Entities.Entities.AppRole", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("AppRoles");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.AppUser", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(0);

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<string>("Gender");

                    b.Property<bool>("IsLocked")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<bool>("LockoutEnabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<DateTimeOffset>("LockoutEnd")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<string>("Salt");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.HasIndex("UserName")
                        .IsUnique();

                    b.ToTable("AppUsers");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.AppUserRole", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AppRoleId");

                    b.Property<int>("AppUserId");

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasAlternateKey("AppUserId", "AppRoleId");

                    b.HasIndex("AppRoleId");

                    b.ToTable("AppUserRoles");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Error", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AppUserId");

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<string>("Message");

                    b.Property<string>("StackTrace");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasIndex("AppUserId");

                    b.ToTable("Errors");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Question", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content");

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<int?>("Order");

                    b.Property<int>("QuestionTypeId");

                    b.Property<int>("SectionId");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasIndex("QuestionTypeId");

                    b.HasIndex("SectionId");

                    b.ToTable("Questions");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Questionnaire", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<string>("Name");

                    b.Property<int?>("Order");

                    b.Property<int?>("SkillId");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("SkillId");

                    b.ToTable("Questionnaires");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.QuestionnaireTerminal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<int>("QuestionnaireId");

                    b.Property<int>("TerminalId");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasAlternateKey("QuestionnaireId", "TerminalId");

                    b.HasIndex("TerminalId");

                    b.ToTable("QuestionnaireTerminals");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.QuestionType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<string>("QuestionTypeName");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.ToTable("QuestionTypes");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Result", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AppUserId");

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<int>("QuestionnaireTerminalId");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<bool>("isComplete")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.HasKey("Id");

                    b.HasIndex("AppUserId");

                    b.HasIndex("QuestionnaireTerminalId");

                    b.ToTable("Results");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.ResultDetail", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AnsweredContent");

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<int>("QuestionId");

                    b.Property<int>("ResultId");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasAlternateKey("ResultId", "QuestionId");

                    b.HasIndex("QuestionId");

                    b.ToTable("ResultDetails");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Section", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<string>("Name");

                    b.Property<int?>("Order");

                    b.Property<int>("QuestionnaireTerminalId");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasIndex("QuestionnaireTerminalId");

                    b.ToTable("Sections");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Skill", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AppUserId");

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<string>("Name");

                    b.Property<int?>("Order");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasIndex("AppUserId");

                    b.ToTable("Skills");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Terminal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<string>("Name");

                    b.Property<int?>("Order");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Terminals");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.UIControl", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CodeName");

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<string>("Name");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.ToTable("UIControls");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.UserSkill", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AppUserId");

                    b.Property<DateTime>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.Property<int>("SkillId");

                    b.Property<DateTime>("UpdatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("DateTime")
                        .HasDefaultValueSql("STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')");

                    b.HasKey("Id");

                    b.HasAlternateKey("AppUserId", "SkillId");

                    b.HasIndex("SkillId");

                    b.ToTable("UserSkills");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.AppUserRole", b =>
                {
                    b.HasOne("PaperlessApp.Entities.Entities.AppRole", "AppRole")
                        .WithMany("AppUserRoles")
                        .HasForeignKey("AppRoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PaperlessApp.Entities.Entities.AppUser", "AppUser")
                        .WithMany("AppUserRoles")
                        .HasForeignKey("AppUserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Error", b =>
                {
                    b.HasOne("PaperlessApp.Entities.Entities.AppUser", "AppUser")
                        .WithMany("Errors")
                        .HasForeignKey("AppUserId");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Question", b =>
                {
                    b.HasOne("PaperlessApp.Entities.Entities.QuestionType", "QuestionType")
                        .WithMany("Questions")
                        .HasForeignKey("QuestionTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PaperlessApp.Entities.Entities.Section", "Section")
                        .WithMany("Questions")
                        .HasForeignKey("SectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Questionnaire", b =>
                {
                    b.HasOne("PaperlessApp.Entities.Entities.Skill", "Skill")
                        .WithMany("Questionnaires")
                        .HasForeignKey("SkillId");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.QuestionnaireTerminal", b =>
                {
                    b.HasOne("PaperlessApp.Entities.Entities.Questionnaire", "Questionnaire")
                        .WithMany("QuestionnaireTerminals")
                        .HasForeignKey("QuestionnaireId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PaperlessApp.Entities.Entities.Terminal", "Terminal")
                        .WithMany("QuestionnaireTerminals")
                        .HasForeignKey("TerminalId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Result", b =>
                {
                    b.HasOne("PaperlessApp.Entities.Entities.AppUser", "AppUser")
                        .WithMany("Results")
                        .HasForeignKey("AppUserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PaperlessApp.Entities.Entities.QuestionnaireTerminal", "QuestionnaireTerminal")
                        .WithMany("Results")
                        .HasForeignKey("QuestionnaireTerminalId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.ResultDetail", b =>
                {
                    b.HasOne("PaperlessApp.Entities.Entities.Question", "Question")
                        .WithMany()
                        .HasForeignKey("QuestionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PaperlessApp.Entities.Entities.Result", "Result")
                        .WithMany("ResultDetails")
                        .HasForeignKey("ResultId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Section", b =>
                {
                    b.HasOne("PaperlessApp.Entities.Entities.QuestionnaireTerminal", "QuestionnaireTerminal")
                        .WithMany("Sections")
                        .HasForeignKey("QuestionnaireTerminalId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.Skill", b =>
                {
                    b.HasOne("PaperlessApp.Entities.Entities.AppUser")
                        .WithMany("Skills")
                        .HasForeignKey("AppUserId");
                });

            modelBuilder.Entity("PaperlessApp.Entities.Entities.UserSkill", b =>
                {
                    b.HasOne("PaperlessApp.Entities.Entities.AppUser", "AppUser")
                        .WithMany()
                        .HasForeignKey("AppUserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PaperlessApp.Entities.Entities.Skill", "Skill")
                        .WithMany("UserSkills")
                        .HasForeignKey("SkillId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
