﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PaperlessApp.Entities.Migrations
{
    public partial class UpdateGeneratedDatetime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AccessFailedCount = table.Column<int>(nullable: false, defaultValue: 0)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false, defaultValue: false),
                    Gender = table.Column<string>(nullable: true),
                    IsLocked = table.Column<bool>(nullable: false, defaultValue: false),
                    LockoutEnabled = table.Column<bool>(nullable: false, defaultValue: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')"),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false, defaultValue: false),
                    Salt = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QuestionTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    QuestionTypeName = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Terminals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    Name = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Terminals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UIControls",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CodeName = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    Name = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UIControls", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppUserRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AppRoleId = table.Column<int>(nullable: false),
                    AppUserId = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUserRoles", x => x.Id);
                    table.UniqueConstraint("AK_AppUserRoles_AppUserId_AppRoleId", x => new { x.AppUserId, x.AppRoleId });
                    table.ForeignKey(
                        name: "FK_AppUserRoles_AppRoles_AppRoleId",
                        column: x => x.AppRoleId,
                        principalTable: "AppRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AppUserRoles_AppUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Errors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AppUserId = table.Column<int>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    Message = table.Column<string>(nullable: true),
                    StackTrace = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Errors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Errors_AppUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AppUserId = table.Column<int>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    Name = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Skills_AppUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Questionnaires",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    Name = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: true),
                    SkillId = table.Column<int>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questionnaires", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Questionnaires_Skills_SkillId",
                        column: x => x.SkillId,
                        principalTable: "Skills",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserSkills",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AppUserId = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    SkillId = table.Column<int>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSkills", x => x.Id);
                    table.UniqueConstraint("AK_UserSkills_AppUserId_SkillId", x => new { x.AppUserId, x.SkillId });
                    table.ForeignKey(
                        name: "FK_UserSkills_AppUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserSkills_Skills_SkillId",
                        column: x => x.SkillId,
                        principalTable: "Skills",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuestionnaireTerminals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    QuestionnaireId = table.Column<int>(nullable: false),
                    TerminalId = table.Column<int>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionnaireTerminals", x => x.Id);
                    table.UniqueConstraint("AK_QuestionnaireTerminals_QuestionnaireId_TerminalId", x => new { x.QuestionnaireId, x.TerminalId });
                    table.ForeignKey(
                        name: "FK_QuestionnaireTerminals_Questionnaires_QuestionnaireId",
                        column: x => x.QuestionnaireId,
                        principalTable: "Questionnaires",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuestionnaireTerminals_Terminals_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Results",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AppUserId = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    QuestionnaireTerminalId = table.Column<int>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    isComplete = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Results", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Results_AppUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Results_QuestionnaireTerminals_QuestionnaireTerminalId",
                        column: x => x.QuestionnaireTerminalId,
                        principalTable: "QuestionnaireTerminals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sections",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    Name = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: true),
                    QuestionnaireTerminalId = table.Column<int>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sections_QuestionnaireTerminals_QuestionnaireTerminalId",
                        column: x => x.QuestionnaireTerminalId,
                        principalTable: "QuestionnaireTerminals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Content = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    Order = table.Column<int>(nullable: true),
                    QuestionTypeId = table.Column<int>(nullable: false),
                    SectionId = table.Column<int>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Questions_QuestionTypes_QuestionTypeId",
                        column: x => x.QuestionTypeId,
                        principalTable: "QuestionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Questions_Sections_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Sections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ResultDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AnsweredContent = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')"),
                    QuestionId = table.Column<int>(nullable: false),
                    ResultId = table.Column<int>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "STRFTIME('%Y-%m-%dT%H:%M:%fZ', 'NOW')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResultDetails", x => x.Id);
                    table.UniqueConstraint("AK_ResultDetails_ResultId_QuestionId", x => new { x.ResultId, x.QuestionId });
                    table.ForeignKey(
                        name: "FK_ResultDetails_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ResultDetails_Results_ResultId",
                        column: x => x.ResultId,
                        principalTable: "Results",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppRoles_Name",
                table: "AppRoles",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppUsers_Email",
                table: "AppUsers",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppUsers_UserName",
                table: "AppUsers",
                column: "UserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppUserRoles_AppRoleId",
                table: "AppUserRoles",
                column: "AppRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Errors_AppUserId",
                table: "Errors",
                column: "AppUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_QuestionTypeId",
                table: "Questions",
                column: "QuestionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_SectionId",
                table: "Questions",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Questionnaires_Name",
                table: "Questionnaires",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Questionnaires_SkillId",
                table: "Questionnaires",
                column: "SkillId");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionnaireTerminals_TerminalId",
                table: "QuestionnaireTerminals",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_Results_AppUserId",
                table: "Results",
                column: "AppUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Results_QuestionnaireTerminalId",
                table: "Results",
                column: "QuestionnaireTerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_ResultDetails_QuestionId",
                table: "ResultDetails",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_Sections_QuestionnaireTerminalId",
                table: "Sections",
                column: "QuestionnaireTerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_Skills_AppUserId",
                table: "Skills",
                column: "AppUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Terminals_Name",
                table: "Terminals",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserSkills_SkillId",
                table: "UserSkills",
                column: "SkillId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppUserRoles");

            migrationBuilder.DropTable(
                name: "Errors");

            migrationBuilder.DropTable(
                name: "ResultDetails");

            migrationBuilder.DropTable(
                name: "UIControls");

            migrationBuilder.DropTable(
                name: "UserSkills");

            migrationBuilder.DropTable(
                name: "AppRoles");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "Results");

            migrationBuilder.DropTable(
                name: "QuestionTypes");

            migrationBuilder.DropTable(
                name: "Sections");

            migrationBuilder.DropTable(
                name: "QuestionnaireTerminals");

            migrationBuilder.DropTable(
                name: "Questionnaires");

            migrationBuilder.DropTable(
                name: "Terminals");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "AppUsers");
        }
    }
}
