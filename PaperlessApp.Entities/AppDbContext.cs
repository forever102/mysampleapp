﻿using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Configurations;
using PaperlessApp.Entities.Entities;

namespace PaperlessApp.Entities
{
    public class AppDbContext : DbContext
    {
        // This one is to make sure OnConfiguring method works correctly.
        //public AppDbContext(DbContextOptions options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {            
            optionsBuilder.UseSqlite("Filename=./PaperlessApp-efCore.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new AppUserConfig(modelBuilder);
            new AppRoleConfig(modelBuilder);
            new AppUserRoleConfig(modelBuilder);
            new ErrorConfig(modelBuilder);
            new SkillConfig(modelBuilder);
            new UserSkillConfig(modelBuilder);
            new QuestionnaireConfig(modelBuilder);
            new TerminalConfig(modelBuilder);
            new QuestionnaireTerminalConfig(modelBuilder);
            new SectionConfig(modelBuilder);
            new QuestionConfig(modelBuilder);
            new UIControlConfig(modelBuilder);
            new QuestionTypeConfig(modelBuilder);
            new ResultDetailConfig(modelBuilder);
            new ResultConfig(modelBuilder);
        }

        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<AppRole> AppRoles { get; set; }
        public DbSet<AppUserRole> AppUserRoles { get; set; }
        public DbSet<Error> Errors { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<UserSkill> UserSkills { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<Terminal> Terminals { get; set; }
        public DbSet<QuestionnaireTerminal> QuestionnaireTerminals { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<UIControl> UIControls { get; set; }
        public DbSet<QuestionType> QuestionTypes { get; set; }
        public DbSet<ResultDetail> ResultDetails { get; set; }
        public DbSet<Result> Results { get; set; }
    }
}
