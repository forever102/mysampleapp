﻿using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;

namespace PaperlessApp.Entities.Configurations
{
    public class ErrorConfig : EntityConfig<Error>
    {
        public ErrorConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<Error>()
                .ToTable("Errors");            
        }
    }
}
