﻿using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace PaperlessApp.Entities.Configurations
{
    public class UIControlConfig : EntityConfig<UIControl>
    {
        public UIControlConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<UIControl>()
                .ToTable("UIControls");
        }
    }
}
