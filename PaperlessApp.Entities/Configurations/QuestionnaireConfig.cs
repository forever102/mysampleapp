﻿using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;

namespace PaperlessApp.Entities.Configurations
{
    public class QuestionnaireConfig : EntityConfig<Questionnaire>
    {
        public QuestionnaireConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<Questionnaire>()
                .ToTable("Questionnaires");

            modelBuilder.Entity<Questionnaire>().HasIndex(q => q.Name).IsUnique();
        }
    }
}
