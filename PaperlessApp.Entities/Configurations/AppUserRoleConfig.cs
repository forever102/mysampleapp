﻿using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperlessApp.Entities.Configurations
{
    public class AppUserRoleConfig : EntityConfig<AppUserRole>
    {
        /*
        * - This is a linking table represents the many-to-many relationship between User and Role.
        */
        public AppUserRoleConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {            
            modelBuilder.Entity<AppUserRole>()
                .ToTable("AppUserRoles")
                .HasAlternateKey(aur => new { aur.AppUserId, aur.AppRoleId });          
        }
    }
}
