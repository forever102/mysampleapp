﻿using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;

namespace PaperlessApp.Entities.Configurations
{
    public class AppRoleConfig : EntityConfig<AppRole>
    {
        public AppRoleConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<AppRole>()
                .ToTable("AppRoles");

            modelBuilder.Entity<AppRole>().HasIndex(u => u.Name).IsUnique();
        }
    }
}
