﻿using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperlessApp.Entities.Configurations
{
    public class SectionConfig : EntityConfig<Section>
    {
        public SectionConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<Section>()
                .ToTable("Sections");
        }
    }
}
