﻿using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;

namespace PaperlessApp.Entities.Configurations
{
    public class ResultDetailConfig : EntityConfig<ResultDetail>
    {
        public ResultDetailConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<ResultDetail>()
                .ToTable("ResultDetails")
                .HasAlternateKey(rd => new { rd.ResultId, rd.QuestionId });
        }
    }
}
