﻿using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace PaperlessApp.Entities.Configurations
{
    public class ResultConfig : EntityConfig<Result>
    {
        public ResultConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<Result>()
                .ToTable("Results");

            modelBuilder.Entity<Result>().Property(r => r.isComplete).HasDefaultValue(false);
        }
    }
}
