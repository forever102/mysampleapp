﻿using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperlessApp.Entities.Configurations
{
    public class AppUserConfig : EntityConfig<AppUser>
    {
        public AppUserConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<AppUser>()
                .ToTable("AppUsers");

            modelBuilder.Entity<AppUser>().Property(au => au.AccessFailedCount).HasDefaultValue(0);
            modelBuilder.Entity<AppUser>().HasIndex(u => u.Email).IsUnique();
            modelBuilder.Entity<AppUser>().Property(au => au.EmailConfirmed).HasDefaultValue(0);
            modelBuilder.Entity<AppUser>().Property(au => au.IsLocked).HasDefaultValue(0);
            modelBuilder.Entity<AppUser>().Property(au => au.LockoutEnabled).HasDefaultValue(0);
            modelBuilder.Entity<AppUser>().Property(au => au.LockoutEnd).HasDefaultValueSql("STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')");
            modelBuilder.Entity<AppUser>().Property(au => au.PhoneNumberConfirmed).HasDefaultValue(0);
            modelBuilder.Entity<AppUser>().HasIndex(u => u.UserName).IsUnique();
        }
    }
}
