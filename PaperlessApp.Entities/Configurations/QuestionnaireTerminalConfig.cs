using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;

namespace PaperlessApp.Entities.Configurations
{
    public class QuestionnaireTerminalConfig : EntityConfig<QuestionnaireTerminal>
    {
        /*
        * - This is a linking table represents the many-to-many relationship between Questionnaire and Terminal.
        */
        public QuestionnaireTerminalConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {            
            modelBuilder.Entity<QuestionnaireTerminal>()
                .ToTable("QuestionnaireTerminals")
                .HasAlternateKey(qt => new { qt.QuestionnaireId, qt.TerminalId });
        }
    }
}
