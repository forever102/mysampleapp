using PaperlessApp.Entities.Abstracts;
using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Entities;

namespace PaperlessApp.Entities.Configurations
{
    public class QuestionTypeConfig : EntityConfig<QuestionType>
    {
        public QuestionTypeConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<QuestionType>()
                .ToTable("QuestionTypes");
        }
    }
}
