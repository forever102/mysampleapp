﻿using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;

namespace PaperlessApp.Entities.Configurations
{
    public class UserSkillConfig : EntityConfig<UserSkill>
    {
        /*
        * - This is a linking table represents the many-to-many relationship between Users and Skills.
        */
        public UserSkillConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<UserSkill>()
                .ToTable("UserSkills")
                .HasAlternateKey(us => new { us.AppUserId, us.SkillId }); ;
        }
    }
}
