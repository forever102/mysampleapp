using Microsoft.EntityFrameworkCore;
using PaperlessApp.Entities.Abstracts;
using PaperlessApp.Entities.Entities;

namespace PaperlessApp.Entities.Configurations
{
    public class TerminalConfig : EntityConfig<Terminal>
    {
        public TerminalConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            modelBuilder.Entity<Terminal>()
                .ToTable("Terminals");

            modelBuilder.Entity<Terminal>().HasIndex(t => t.Name).IsUnique();
        }
    }
}
