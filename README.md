# README #

This project includes source code of cross-platform application (Ionic 1 using AngularJS) and .Net Core (create SQLite DB using EF 7).

### What is this repository for? ###

This repo is for showing my coding style.

### How do I get set up? ###

You can run: 'npm install' and 'bower install' to see the packages I used in this app. But to make the app run, you should edit some stuffs. Please check my code to see it.

### Contribution guidelines ###

I didn't create much Unit and E2E testings.

### Who do I talk to? ###

For everyone.